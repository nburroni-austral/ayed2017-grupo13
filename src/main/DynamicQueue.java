package main;

import struct.istruct.*;

import java.util.NoSuchElementException;

/**
 * Created by Tincho on 10-Apr-17.
 */
public class DynamicQueue<Q> implements Queue<Q>{

    private class Node<Q> {
        private Q element;
        private Node<Q> next;
    }


    private Node<Q> head;
    private Node<Q> tail;
    public int size;

    public void enqueue(Q e) {
        Node oldTail = tail;
        tail = new Node<>();
        tail.element = e;
        if (isEmpty()) head = tail;
        else oldTail.next = tail;
        size++;
    }

    public Q dequeue() {
        if (isEmpty()) throw new NoSuchElementException("Queue is empty.");
        Q e = head.element;
        head = head.next;
        size--;
        return e;
    }

    public Q peek() {
        if (isEmpty()) throw new NoSuchElementException("Queue is empty.");
        return head.element;
    }

    public boolean isEmpty() {
        return head == null;
    }

    @Override
    public int length() {
        return size;
    }

    public int size() {
        return size;
    }

    @Override
    public void empty() {
        head = tail;
    }
}
