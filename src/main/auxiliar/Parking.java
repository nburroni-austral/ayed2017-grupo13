package main.auxiliar;

import main.Car;
import main.StaticStack;

/**
 * Created by Florencia on 3/25/17.
 */
public class Parking{
    private StaticStack<Car> parking;
    private int earnings = 0;

    public Parking(){
        parking = new StaticStack<>(50);
    }

    public void addCar(Car aCar){
        parking.push(aCar);
    }

    public void removeCar(Car aCar){
        StaticStack<Car> auxStack = new StaticStack<>(parking.size());
        for(int i = 0; i < parking.size(); i++){
            Car aux = parking.peek();
            auxStack.push(aux);
            parking.pop();
            if(parking.peek().getPatent().equals(aCar.getPatent())){
                for (int j = 0; j < 5 ; j++) {
                    parking.pop();
                    earnings += 5;
                }
            }
        }
    }

    public int closeParking(){
        if(parking.isEmpty()){
            return earnings;
        } else {
            earnings += parking.size()*5;
            parking.empty();
            return earnings;
        }
    }
}
