package main.auxiliar;

/**
 * Created by Florencia on 3/13/17.
 */
public class Llamada {
    private long emisor;
    private long receptor;
    private int duration;

    public Llamada(long emisor, long receptor, int duration) {
        this.emisor = emisor;
        this.receptor = receptor;
        this.duration = duration;
    }

    /*@ pure @*/
    public int hacerLlamada(){
        //emisor.llamar(receptor);
        return duration;
    }
    /*@ ensure \result > 0
    @*/

    /*@ assignable emisor
        requires emisor.char() > 2
    @*/
    public void setEmisor(long newEmisor){
         emisor = newEmisor;
    }
}
