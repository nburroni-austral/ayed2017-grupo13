package main.auxiliar;

/**
 * Created by Florencia on 3/7/17.
 */
public class Search {
    //precond: con A un conjunto de enteros no necesariamente ordenados y k un elemento
    //postcond: boolean k pertenece a A o no

    public boolean secuencialSearchBoolean(int[] array, int k){

        for(int i = 0; i < array.length; i++){
            if(k == array[i]){
                return true;
            }
        }

        return false;
    }

    public int secuencialSearch(int[] array, int k) throws Exception {
        for(int i = 0; i < array.length; i++) {
            if (k == array[i]) {
                return i;
            }
        }
        return -1;
    }

    private boolean binarySearchBoolean(int[] array, int number, int first, int last){

        if(first > last){ //condicion de corte
            return false;
        }

        int middleIndex = (first + last)/2;

        if(number == array[middleIndex]){
            return true;
        }
        else if(number < array[middleIndex]){
            return binarySearchBoolean(array, number, 0, middleIndex-1);
        }
        else{
            return binarySearchBoolean(array, number, middleIndex + 1, last);
        }
    }

    public int binarySearch(int[] array, int number){
        return binarySearch(array, number, 0, array.length-1);
    }

    private int binarySearch(int[] array, int number, int first, int last) {

        if (first > last) { //condicion de corte
            return -1;
        }

        int middleIndex = (first + last) / 2;

        if (number == array[middleIndex]) {
            return middleIndex;
        } else if (number < array[middleIndex]) {
            return binarySearch(array, number, 0, middleIndex - 1);
        } else {
            return binarySearch(array, number, middleIndex + 1, last);
        }
    }
}
