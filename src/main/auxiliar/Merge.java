package main.auxiliar;

import java.util.Arrays;

/**
 * Created by Florencia on 3/13/17.
 */
public class Merge {
    private int[] array1;

    public Merge(int[] array1) {
        this.array1 = array1;
    }

    public int[] ordenar(int[] array1){
        int temp = 0;
        for(int i = 0; i< array1.length -1; i++){
            for(int j = 0; j < array1.length -1 -i; j++){
                if(array1[j] > array1[j+1]){
                    temp = array1[j];
                    array1[j] = array1[j+1];
                    array1[j+1] = temp;
                }
            }
        }

        return array1;
    }

    public int[] mergeAll(int[] arrayA, int[] arrayB){
        int i = 0;
        int j = 0;
        Merge.this.ordenar(arrayA);
        Merge.this.ordenar(arrayB);
        int tamano = arrayA.length + arrayB.length;
        int[] arrayC = new int[tamano];
        for (int k = 0; k < arrayC.length+1; k++){
            if (i>=arrayA.length ){
                for (int g = j; g < arrayB.length;g++){
                    arrayC[k] = arrayB[g];
                    j++;
                }
            }else if (j>=arrayB.length){
                for (int g = i; g < arrayA.length;g++){
                    arrayC[k] = arrayA[g];
                    i++;
                }
            }else {
                if (arrayA[i]>=arrayB[j]){
                    arrayC[k] = arrayB[j];
                    j++;
                }else{
                    arrayC[k] = arrayA[i];
                    i++;
                }
            }
        }
        return arrayC;
    }

    @Override
    public String toString() {
        return "Merge{" +
                "array1=" + Arrays.toString(array1) +
                '}';
    }
}
