package main.auxiliar;

/**
 * Created by Florencia on 3/7/17.
 */
public class SearchMain {
    public static void main(String[] args) throws Exception {
        int[] example = {3,2,5,4,8};
        int[] example2 = {6,1,7,9};
        int[] example3;
        int k = 8;

        Search lookUp = new Search();
        System.out.println(lookUp.binarySearch(example, k));
        System.out.println(lookUp.secuencialSearch(example, k));
        Merge merge1 = new Merge(example);
        example3 = merge1.mergeAll(example,example2);
        Merge merge = new Merge(example3);
        System.out.println(merge.toString());



    }
}
