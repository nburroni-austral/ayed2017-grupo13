package main.auxiliar;

import main.stacks.DynamicStack;

import java.io.File;
import java.io.IOException;

/**
 * Created by Florencia on 3/27/17.
 */
public class Lexicografico {
    private String text;
    private DynamicStack<Character> stack;

    public Lexicografico(File file) throws IOException {
        ReadFile fileReader = new ReadFile();
        text = fileReader.readFile(file);
        stack = new DynamicStack();
    }

    public boolean charactersClose(){
        return charactersClosePrivate();
    }

    private boolean charactersClosePrivate(){
        char[] array = text.toCharArray();
        for (int i = 0; i < array.length; i++) {
            if(open(array[i])){
                stack.push(array[i]);
            } else if(close(array[i])){
                if (stack.peek() == '{' && array[i] == '}'){
                    stack.pop();
                }else if (stack.peek() == '(' && array[i] == ')'){
                    stack.pop();
                }else if (stack.peek() == '[' && array[i] == ']'){
                    stack.pop();
                }else{
                    stack.push(array[i]);
                }
            }
        }
        return stack.isEmpty();
    }

    private boolean open(char a){
        if (a == '{' || a =='(' || a =='[' ) {
            return true;
        }
        return false;
    }

    private boolean close(char a){
        if (a == '}' || a == ')' || a == ']') {
            return true;
        }
        return false;
    }
}
