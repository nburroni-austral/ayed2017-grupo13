package main.practicaParcial.MetroVias;

import main.practicaParcial.Scanner;
import main.StaticList;

import java.util.Random;

/**
 * Created by Florencia on 4/19/17.
 */
public class Metrovias {
    private StaticList<Window> windows;

    public Metrovias(){
        Scanner scanner = new Scanner();
        int capacity = scanner.getInt("Enter amount of windows: ");
        if(capacity > 3 || capacity < 10){
            windows = new StaticList<>(capacity);
            setWindows(capacity);
        } else {
            System.out.println("Amount of windows must be between 3 and 10.");
        }
    }

    public void simulate(){
        int time = 0;
        Random random = new Random();
        while (time < 57600){
            if(time%10 == 0){
                int amountClients = random.nextInt(5);
                newClients(amountClients, time);
            }
            for(int i = 0; i < windows.size(); i++){
                windows.goTo(i);
                windows.getActual().goToWorkConditional(time);
            }
            if(time == 57670){
                for (int i = 0; i < windows.size() ; i++) {
                    windows.goTo(i);
                    windows.getActual().goToWorkFinal(time);
                }
            }
            time++;
        }
        for (int i = 0; i < windows.size(); i++) {
            windows.goTo(i);
            windows.getActual().getResults();
        }
    }

    private void newClients(int amountClients, int time){
        for (int i = 0; i < amountClients; i++){
            Random random = new Random();
            int choice = random.nextInt(windows.size());
            windows.goTo(choice);
            windows.getActual().waitingList.enqueue(new Client(time));

        }
    }

    private void setWindows(int capacity){
        for (int i = 0; i < capacity; i++) {
            windows.insertNext(new Window());
        }
    }
}
