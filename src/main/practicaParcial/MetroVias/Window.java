package main.practicaParcial.MetroVias;

import main.DynamicQueue;

import java.util.Random;

/**
 * Created by Florencia on 4/19/17.
 */
public class Window {
    public float savings = 0;
    public boolean working;
    public DynamicQueue<Client> waitingList;
    public int clientsAttended = 0;
    public int freeTime = 0;
    public int timeLost = 0;

    public Window(){
        working = false;
        waitingList = new DynamicQueue<>();
    }

    public void goToWorkConditional(int time){
        Random random = new Random();
        if(random.nextFloat() <= 0.3){
            if(waitingList.isEmpty()){
                freeTime += 10;
            } else {
                timeLost += waitingList.peek().getEstimatedTime();
                waitingList.peek().dequeueClient(time);
                waitingList.dequeue();
                clientsAttended++;
            }
        }
    }

    public void goToWorkFinal(int time){
        timeLost += waitingList.peek().getEstimatedTime();
        waitingList.peek().dequeueClient(time);
        waitingList.dequeue();
        clientsAttended++;
    }


    public void getResults(){
        System.out.println("Plata recaudada: "+ calculateSavings() + "$");
        System.out.println("Tiempo de ocio: "+ getFreeTime()+ " minutos");
        System.out.println("Tiempo de espera: " + getTimeLost()+ " minutos");
    }

    private float calculateSavings(){
        savings = (float) (clientsAttended*0.7);
        return savings;
    }

    private int getFreeTime() {
        return freeTime/60;
    }

    private int getTimeLost(){
        return timeLost/60;
    }
}
