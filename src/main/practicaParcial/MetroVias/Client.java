package main.practicaParcial.MetroVias;

/**
 * Created by Florencia on 4/11/17.
 */
public class Client {
    private int arrivalTime;
    private int leavingTime;

    Client(int n){
        arrivalTime = n;
    }

    public void dequeueClient(int time){
        leavingTime = time;
    }

    public int getEstimatedTime(){
        return arrivalTime - leavingTime;
    }
}
