package main.practicaParcial.CuartoOscuro;

/**
 * Created by Florencia on 4/25/17.
 */
public class PresidenteMesa {
    private Mesa mesa;
    // tiempo total que tarda cada uno y espera?

    public PresidenteMesa(Mesa mesa) {
        this.mesa = mesa;
    }

    public void reponerBoletas(){ //seguir paso a paso
        for (int i = 0; i < mesa.getCuartoOscuro().getBoletasList().size(); i++) {
            mesa.getCuartoOscuro().getBoletasList().goTo(i);
            int maxBoletas = 200;
            int actualBoletas = mesa.getCuartoOscuro().getBoletasList().getActual().getBoletasPila().size(); //-1??
            int agregarBoletas = maxBoletas - actualBoletas;

            while(mesa.getCuartoOscuro().getBoletasList().getActual().getBoletasPila().size()-1 < 200){
                mesa.getCuartoOscuro().getBoletasList().goTo(i);
                mesa.getCuartoOscuro().getBoletasList().getActual().getBoletasPila().push(new Boleta(mesa.getCuartoOscuro().getBoletasList().getActual().getPartido()));
            }
            System.out.println("Boletas agregadas: " + agregarBoletas);
        }
    }

}
