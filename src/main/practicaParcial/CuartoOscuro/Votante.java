package main.practicaParcial.CuartoOscuro;

import java.util.Date;
import java.util.Random;

/**
 * Created by Florencia on 4/25/17.
 */
public class Votante {
    private int dni;
    private Boleta voto;
    private Mesa mesa;
    private Date llegadaMesa;
    private Date llegadaCuartoOscuro;

    public Votante(Mesa mesa) {
        this.mesa = mesa;
        Random random = new Random();
        dni = random.nextInt(99999999);
        if(dni < 999999){
            dni = random.nextInt(99999999);
        }

        llegadaMesa = new Date();
        llegadaCuartoOscuro = null;
    }

    public double esperaEnLinea(){ //devuelve segundos
        Date tiempoActual = new Date();
        return (tiempoActual.getTime() - llegadaMesa.getTime()) / 1000; // a quien le paso este double?? -> presidente de mesa..?
    }

    //tiempo @ cuarto oscuro


    public void entrarCuartoOscuro(){
        Date entradaCuartoOscuro = new Date();
    }

    public void salirCuartoOscuro(){
        Random random = new Random();
        Boleta voto = null;
        int eleccion = random.nextInt(5)+1;
        for (int i = 0; i < 5; i++) {
            if(i == eleccion){
                mesa.getCuartoOscuro().getBoletasList().goTo(i);
                voto = mesa.getCuartoOscuro().getBoletasList().getActual();
            }
        }
        setVoto(voto);
        mesa.getUrna().agregarVoto(voto);
        mesa.getCuartoOscuro().setOcupado(false);
    }

    public void setVoto(Boleta voto) {
        this.voto = voto;
    }
}
