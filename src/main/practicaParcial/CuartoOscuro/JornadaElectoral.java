package main.practicaParcial.CuartoOscuro;

import main.practicaParcial.Scanner;

/**
 * Created by Florencia on 4/25/17.
 */
public class JornadaElectoral {

    public static void main(String[] args) {
        Mesa mesa = new Mesa();
        mesa.crearPartidos();
        int opcion = Scanner.getInt("Ingrese una opcion: balbla");
//                System.out.println("Operations:\n" +
//                        " 1- New person arrives to the table\n" +
//                        " 2- Person enter voting booth.\n" +
//                        " 3- Person leaves voting booth with his vote.\n" +
//                        " 4- Restock of all voting tickets.\n" +
//                        " 5- End simulation. \n");
//        System.out.print("Enter a operation: ");
//        opcion = Scanner.getInt(" ");

        while(opcion < 5){
            switch (opcion){
                case 1:
                    mesa.ingresarPersona();
                    break;

                case 2:
                    if(mesa.getCuartoOscuro().isOcupado()){
                        break;
                    }
                    mesa.getFilaDeEspera().peek().entrarCuartoOscuro();
                    break;

                case 3:
                    if(!mesa.getCuartoOscuro().isOcupado()){
                        break;
                    }
                    mesa.getFilaDeEspera().peek().salirCuartoOscuro();
                    break;

                case 4:
                    if(mesa.getCuartoOscuro().isOcupado()){
                        break;
                    }
                    mesa.getPresidenteMesa().reponerBoletas();
            }
        }
    }

}
