package main.practicaParcial.CuartoOscuro;

import main.DynamicList;

/**
 * Created by Florencia on 4/25/17.
 */
public class Urna {
    private DynamicList<Boleta> votos;
    private Mesa mesa;

    public Urna(Mesa mesa) {
        votos = new DynamicList<>();
        this.mesa = mesa;
    }

    public void agregarVoto(Boleta voto){
        votos.insertNext(voto);

    }

    public void contarVotos(){
        DynamicList<Boleta> partidosList = mesa.getCuartoOscuro().getBoletasList();//Dynamic o static?
        for (int i = 0; i < votos.size(); i++) {
            int countPartido = 0;
            votos.goTo(i);
            for (int j = 0; j < partidosList.size(); j++) {
                partidosList.goTo(i);
                if(votos.getActual().getPartido().equals(partidosList.getActual())){
                    countPartido++;
                }
            }
            System.out.println(countPartido);
        }

    }

    public void ultimosVotos(){

    }
}
