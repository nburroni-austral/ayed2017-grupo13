package main.practicaParcial.CuartoOscuro;

import main.stacks.DynamicStack;

import java.util.Random;

/**
 * Created by Florencia on 4/25/17.
 */
public class Boleta {
    private int id;
    private String partido;
    private DynamicStack<Boleta> boletasPila;

    public Boleta(String partido) {
        Random random = new Random();
        id = random.nextInt(999999);
        if(id < 99999){
            id = random.nextInt(999999);
        }
        this.partido = partido;
        boletasPila = new DynamicStack<>();
    }

    public int getId() {
        return id;
    }

    public String getPartido() {
        return partido;
    }

    public DynamicStack<Boleta> getBoletasPila() {
        return boletasPila;
    }

    @Override
    public String toString() {
        return "Boleta{" +
                "id=" + id +
                ", partido='" + partido + '\'' +
                '}';
    }
}
