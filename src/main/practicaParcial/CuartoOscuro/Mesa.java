package main.practicaParcial.CuartoOscuro;

import main.DynamicQueue;
import main.practicaParcial.Scanner;
import main.StaticList;

/**
 * Created by Florencia on 4/25/17.
 */
public class Mesa {
    private CuartoOscuro cuartoOscuro;
    private DynamicQueue<Votante> filaDeEspera;
    private Urna urna;
    private PresidenteMesa presidenteMesa;
    private StaticList<String> partidos = new StaticList<>();

    public Mesa() {
        cuartoOscuro = new CuartoOscuro();
        filaDeEspera = new DynamicQueue<>();
        urna = new Urna(this);
        presidenteMesa = new PresidenteMesa(this);
    }

    public void ingresarPersona(){
        Votante votante = new Votante(this);
        if(!filaDeEspera.isEmpty()){
            filaDeEspera.enqueue(votante);
        }
        //empezar tiempo de espera.
    }

    public void crearPartidos(){
        for (int i = 0; i < 5; i++) {
            partidos.goTo(i);
            partidos.insertNext(Scanner.getString("Partido: "));
            getCuartoOscuro().getBoletasList().insertNext(new Boleta(partidos.getActual()));
            System.out.println(getCuartoOscuro().getBoletasList().size());
        }
    }

    public CuartoOscuro getCuartoOscuro() {
        return cuartoOscuro;
    }

    public DynamicQueue<Votante> getFilaDeEspera() {
        return filaDeEspera;
    }

    public Urna getUrna() {
        return urna;
    }

    public PresidenteMesa getPresidenteMesa() {
        return presidenteMesa;
    }
}
