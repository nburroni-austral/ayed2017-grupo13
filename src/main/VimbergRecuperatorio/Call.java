package main.VimbergRecuperatorio;

import java.util.Date;

/**
 * Created by Florencia on 6/27/17.
 */
public class Call {
    private int clientCod;
    private float amount;
    private int fees; //# de cuotas
    private Date timeArrivedAtWaitingQueue;
    private Date timeWhenAttended;

    public Call(int clientCod, float amount, int fees) {
        this.clientCod = clientCod;
        this.amount = amount;
        this.fees = fees;
        timeArrivedAtWaitingQueue = null;
        timeWhenAttended = null;
    }

    public void enterCall(){
        timeWhenAttended = new Date();
    }

    public int getClientCod() {
        return clientCod;
    }

    public float getAmount() {
        return amount;
    }

    public int getFees() {
        return fees;
    }

    public Date getTimeArrivedAtWaitingQueue() {
        return timeArrivedAtWaitingQueue;
    }

    public Date getTimeWhenAttended() {
        return timeWhenAttended;
    }

    public void arrivedAtWaitingQueue() {
        timeArrivedAtWaitingQueue = new Date();
    }
}
