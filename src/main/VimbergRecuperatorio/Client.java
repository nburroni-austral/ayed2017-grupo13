package main.VimbergRecuperatorio;

/**
 * Created by Florencia on 6/27/17.
 */
public class Client implements Cliente {
    private String name;
    private int cod;
    private float balance;
    private float availableCredit;
    private boolean isEnabled;

    public Client(String name, int cod, float balance, float availableCredit){
        this.name = name;
        this.cod = cod;
        this.balance = balance;
        this.availableCredit = availableCredit;
        isEnabled = true;
    }

    public void setBalance(float amount){
        balance -= amount;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public int getCod() {
        return cod;
    }

    @Override
    public float getBalance() {
        return balance;
    }

    @Override
    public void setAmount(float amount) {
        balance = amount;
    }

    @Override
    public float getAvailableCredit() {
        return availableCredit;
    }

    @Override
    public boolean isEnabled() {
        return isEnabled;
    }

    @Override
    public int compareTo(Client client) {
        if(this.getCod() < client.getCod()){
            return -1;
        } else if(this.getCod() > client.getCod()){
            return 1;
        } else {
            return 0;
        }
    }
}
