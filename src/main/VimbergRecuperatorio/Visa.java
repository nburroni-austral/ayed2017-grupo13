package main.VimbergRecuperatorio;

import main.DynamicList;
import main.sortedList.DynamicSortedList;

/**
 * Created by Florencia on 6/27/17.
 */
public class Visa {
    private DynamicSortedList<Client> clientList;
    private TelephoneService telephoneService;
    private DynamicList<Call> everyCall;

    public Visa(DynamicSortedList<Client> clientList, int n){
        this.clientList = clientList;
        telephoneService = new TelephoneService(n);
        everyCall = new DynamicList<>();
    }

    public String enterCall(Call call){
        if(everyCall.isVoid()){
            everyCall.insertNext(call);
        } else {
            everyCall.goTo(everyCall.size() -1);
            everyCall.insertNext(call);
        }
        return telephoneService.enterCall(call, checkAcceptance(call));
    }

    public DynamicSortedList<Client> getClientList() {
        return clientList;
    }

    public TelephoneService getTelephoneService() {
        return telephoneService;
    }

    public String checkAcceptance(Call call){
        Client client = null;
        int i = 0;
        while(client == null){
            clientList.goTo(i);
            if (clientList.getActual().getCod() == call.getClientCod()){
                client = clientList.getActual();
            }
            i++;
        }
        if(client.getBalance() + call.getAmount() < client.getAvailableCredit()){
            client.setBalance(call.getAmount());
            return "accepted";
        } else {
            return "declined";
        }
    }

    public DynamicList<Call> lastCalls(){
        DynamicList<Call> lastCalls = new DynamicList<>();
        for (int i = everyCall.size() - 1; i > 0 && i >= everyCall.size() - 10 ; i--) {
            everyCall.goTo(i);
           // if(lastCalls.isVoid()){
             lastCalls.insertNext(everyCall.getActual());
           // }
//            else {
//                lastCalls.goTo(i);
//                lastCalls.insertNext(everyCall.getActual());
//            }
        }
        return lastCalls;
    }
}
