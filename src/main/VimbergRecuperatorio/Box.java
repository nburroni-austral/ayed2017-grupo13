package main.VimbergRecuperatorio;

import main.DynamicStack;

import java.util.Date;

/**
 * Created by Florencia on 6/27/17.
 */
public class Box {
    private int amountClientsAttended;
    private DynamicStack<Call> stackCallAttended;
    private boolean isOccupied;
    private Call callAtBox;
    private Date timeStartedWorking;

    public Box() {
        amountClientsAttended = 0;
        stackCallAttended = new DynamicStack<>();
        isOccupied = false;
        callAtBox = null;
        timeStartedWorking = null;
    }

    public void freeLine(){
        stackCallAttended.push(callAtBox);
        callAtBox = null;
        isOccupied = false;
        amountClientsAttended++;
    }

    public int getAmountClientsAttended() {
        return amountClientsAttended;
    }

    public DynamicStack<Call> getStackClientsAttended() {
        return stackCallAttended;
    }

    public boolean isOccupied() {
        return isOccupied;
    }

    public void setCallAtBox(Call call) {
        callAtBox = call;
        timeStartedWorking = new Date();
    }

    public DynamicStack<Call> getStackCallAttended() {
        return stackCallAttended;
    }

    public Call getCallAtBox() {
        return callAtBox;
    }

    public Date getTimeStartedWorking() {
        return timeStartedWorking;
    }
}
