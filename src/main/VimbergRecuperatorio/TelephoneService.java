package main.VimbergRecuperatorio;

import main.DynamicQueue;
import main.StaticList;

/**
 * Created by Florencia on 6/27/17.
 */
public class TelephoneService {
    private StaticList<Box> telephoneLines;
    private DynamicQueue<Call> waitingQueue;

    public TelephoneService(int n){
        telephoneLines = new StaticList<>(n);
        telephoneLines.goTo(0);
        for (int i = 0; i < n; i++) {
            telephoneLines.insertNext(new Box());
        }
        waitingQueue = new DynamicQueue<>();
    }

    public String enterCall(Call call, String acceptance){
        String result = "";
        for (int i = 0; i < telephoneLines.size(); i++) {
            telephoneLines.goTo(i);
            if(!telephoneLines.getActual().isOccupied()){
                telephoneLines.getActual().setCallAtBox(call);
                call.enterCall();
                //result = acceptance;
                return acceptance;
            }
//            else {
//
//            }
        }
        call.arrivedAtWaitingQueue();
        waitingQueue.enqueue(call);
        return result;
    }

    public StaticList<Box> getTelephoneLines() {
        return telephoneLines;
    }

    public DynamicQueue<Call> getWaitingQueue() {
        return waitingQueue;
    }
}
