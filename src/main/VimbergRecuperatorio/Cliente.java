package main.VimbergRecuperatorio;

/**
 * Created by Florencia on 6/27/17.
 */
public interface Cliente extends Comparable<Client> {
    public String getName();
    public int getCod();
    public float getBalance();
    public void setAmount(float amount);
    public float getAvailableCredit();
    public boolean isEnabled();
}
