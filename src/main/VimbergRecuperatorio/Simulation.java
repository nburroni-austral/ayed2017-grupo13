package main.VimbergRecuperatorio;

import main.parcialVimberg.Scanner;
import main.sortedList.DynamicSortedList;

import java.util.Date;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by Florencia on 6/27/17.
 */
public class Simulation {
    public static void main(String[] args) {
        Visa visa = new Visa(createClientList(), Scanner.getInt("Enter amount of lines: "));

        Result result = new Result(visa);
        int option = 0;

        while (option != 3){
            option = Scanner.getInt(" 1. Enter new call. " +
                    "\n 2. Get results." +
                    "\n 3. Exit " +
                    "\n Choose an option: ");
            switch (option){
                case 1:
                    String acceptance = visa.enterCall(generateCall(visa));
                    System.out.println("The purchase was " + acceptance);
                    break;
                case 2:
                    result.printResults();
                    break;
                case 3:
                    break;
                default:
                    System.out.println("Please enter a valid option");
            }
            freeLines(visa);
        }
    }

    public static Call generateCall(Visa visa){
        return new Call(getRandomClient(visa), getRandomAmount(), getAmountOfFees());
    }

    public static int getRandomClient(Visa visa){
        int randomClientCod = ThreadLocalRandom.current().nextInt(0, visa.getClientList().size());  //da entre 0 y 10 inclusive
       visa.getClientList().goTo(randomClientCod);
       return visa.getClientList().getActual().getCod();
    }

    public static float getRandomAmount(){
        return ThreadLocalRandom.current().nextFloat() * 1000;
    }

    public static int getAmountOfFees(){
        return ThreadLocalRandom.current().nextInt(0, 12 + 1); // entre 0 y 12 inclusive
    }

    public static DynamicSortedList<Client> createClientList(){
        DynamicSortedList<Client> clientList = new DynamicSortedList<>();
        for (int i = 0; i < 5; i++) {
           Client client = generateClient();
           clientList.insert(client);
        }
        return clientList;
    }

    public static void freeLines(Visa visa){
        for (int i = 0; i < visa.getTelephoneService().getTelephoneLines().size(); i++) {
            visa.getTelephoneService().getTelephoneLines().goTo(i);

            if(visa.getTelephoneService().getTelephoneLines().getActual().isOccupied() &&
                    getCurrentTime().getTime() - visa.getTelephoneService().getTelephoneLines().getActual().getTimeStartedWorking().getTime() >= 300) {
                visa.getTelephoneService().getTelephoneLines().getActual().freeLine();
            }
        }
    }

    public static Date getCurrentTime(){
        return new Date();
    }

    public static Client generateClient(){
        return new Client(Scanner.getString("Enter clients name: "),
                randomCod(), randomBalance(), randomAvailableCredit());
    }

    public static int randomCod(){
        return ThreadLocalRandom.current().nextInt(0, 1000 + 1);  //da entre 0 y 1000 inclusive
    }

    public static float randomBalance(){
        return ThreadLocalRandom.current().nextFloat() * 1000;  // < 0 y > 1000
    }

    public static float randomAvailableCredit(){
        return ThreadLocalRandom.current().nextFloat() * 10000;  // < 0 y > 10000
    }
}
