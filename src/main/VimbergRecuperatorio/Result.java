package main.VimbergRecuperatorio;

import main.DynamicList;
import main.sortedList.StaticSortedList;

/**
 * Created by Florencia on 6/27/17.
 */
public class Result {
    private Visa visa;

    public Result(Visa visa){
        this.visa = visa;
    }

    public void printResults(){
        //en que horario hubo mas llamadas y cuantas hubo en ese momento

        //# llamadas atendidas por cada linea
        StaticSortedList<Integer> callsAttendedPerLine = new StaticSortedList<>(visa.getTelephoneService().getTelephoneLines().size());
        for (int i = 0; i < visa.getTelephoneService().getTelephoneLines().size(); i++) {
            visa.getTelephoneService().getTelephoneLines().goTo(i);
            callsAttendedPerLine.insert(visa.getTelephoneService().getTelephoneLines().getActual().getAmountClientsAttended());
        }
        for (int i = 0; i < callsAttendedPerLine.size(); i++) {
            callsAttendedPerLine.goTo(i);
            System.out.println("Calls attended by line: " + callsAttendedPerLine.getActual());
        }

        //mostrar ultimas diez llamadas
        lastCalls();
    }

    public void lastCalls(){
        DynamicList<Call> lastCalls = visa.lastCalls();
        for (int i = 0; i < lastCalls.size(); i++) {
            lastCalls.goTo(i);
            System.out.println((i+1) + " call " + lastCalls.getActual().getClientCod());
        }
    }
}
