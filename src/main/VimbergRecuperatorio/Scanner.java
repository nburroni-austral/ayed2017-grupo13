package main.VimbergRecuperatorio;

import java.lang.System;

/**
 * Created by Florencia on 4/11/17.
 */
    class Scanner {
        private static final java.util.Scanner scanner;

        private Scanner() {
        }

        public static String getString(String message) {
            java.lang.System.out.print(message);
            String result = scanner.nextLine().trim();
            if(result.isEmpty()) {
                java.lang.System.out.println("Please enter a text.");
                return getString(message);
            } else {
                return result;
            }
        }

        public static char getChar(String message) {
            return getString(message).charAt(0);
        }

        public static int getInt(String message) {
            java.lang.System.out.print(message);

            try {
                return Integer.parseInt(scanner.nextLine());
            } catch (NumberFormatException var2) {
                java.lang.System.out.println("Please enter an integer.");
                return getInt(message);
            }
        }

        public static long getLong(String message) {
            java.lang.System.out.print(message);

            try {
                return Long.parseLong(scanner.nextLine());
            } catch (NumberFormatException var2) {
                java.lang.System.out.println("Please enter a long.");
                return getLong(message);
            }
        }

        public static float getFloat(String message) {
            java.lang.System.out.print(message);

            try {
                return Float.parseFloat(scanner.nextLine());
            } catch (NumberFormatException var2) {
                java.lang.System.out.println("Please enter a float.");
                return getFloat(message);
            }
        }

        public static double getDouble(String message) {
            java.lang.System.out.print(message);

            try {
                return Double.parseDouble(scanner.nextLine());
            } catch (NumberFormatException var2) {
                java.lang.System.out.println("Please enter a double.");
                return getDouble(message);
            }
        }

        public static void main(String[] args) {
            String text = getString("Enter some text: ");
            java.lang.System.out.println("The entered text was: " + text);
            char c = getChar("Enter a char: ");
            java.lang.System.out.println("The entered char is: " + c);
            int i = getInt("Enter an int: ");
            java.lang.System.out.println("The entered int is: " + i);
            long l = getLong("Enter a long: ");
            java.lang.System.out.println("The entered long is: " + l);
            float f = getFloat("Enter a float: ");
            java.lang.System.out.println("The entered float is: " + f);
            double d = getDouble("Enter a double: ");
            java.lang.System.out.println("The entered double is: " + d);
        }

        static {
            scanner = new java.util.Scanner(System.in);
        }
    }
