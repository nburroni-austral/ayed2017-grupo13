package main.TPEquipos;

import java.util.ArrayList;

/**
 * Created by Florencia on 4/17/17.
 */
public class League {
    private ArrayList<Game> playedGames;
    private ArrayList<Team> teams = new ArrayList<>();

    public League(ArrayList<Game> playedGames) {
        this.playedGames = playedGames;
        setTeams();
    }

    public ArrayList<Integer> getResults(){
        ArrayList<Integer> results = new ArrayList<>();
        setGames();
        int position = 0;
        while (!finishLeague()){
            while (!outOfBounds()&& position<playedGames.size()){
                if(playedGames.get(position).possibleResults.peek().equals(0)){
                    playedGames.get(position).getTeamA().score--;
                    playedGames.get(position).getTeamB().score--;
                } else if(playedGames.get(position).possibleResults.peek().equals(2)){
                    playedGames.get(position).getTeamB().score -= 3;
                } else {
                    playedGames.get(position).getTeamA().score -= 3;
                }
                playedGames.get(position).getTeamA().games--;
                playedGames.get(position).getTeamB().games--;
                position++;
            }
            if (outOfBounds()) {
                getPreviousStack(--position);
            }
        }
        for (int i = 0; i < playedGames.size(); i++) {
            results.add( playedGames.get(i).possibleResults.peek());
        }
        System.out.println(results);
        return results;
    }

    public void setGames(){
        for (int i = 0; i < playedGames.size() ; i++) {
            playedGames.get(i).getGames();
        }
    }

    public void setTeams(){
        for (int i = 0; i < playedGames.size(); i++) {
            if(!teams.contains(playedGames.get(i).getTeamA())){
                teams.add(playedGames.get(i).getTeamA());
            }

            if(!teams.contains(playedGames.get(i).getTeamB())){
                teams.add(playedGames.get(i).getTeamB());
            }
        }
    }

    private boolean finishLeague(){
        boolean finish = false;
        for (int i = 0; i <teams.size() ; i++) {
            if (teams.get(i).getScore() == 0){
                finish = true;
            }else {finish = false;}
        }
        return finish;
    }

    private boolean outOfBounds(){
        boolean result = false;
        for (int i = 0; i < teams.size(); i++) {
            if (teams.get(i).getScore() < 0){
                result = true;
            }
        }
        return result;
    }

    private void getPreviousStack(int position){
        if (playedGames.get(position).possibleResults.size() > 1) {
            if (playedGames.get(position).possibleResults.peek().equals(0)) {
                playedGames.get(position).getTeamA().score++;
                playedGames.get(position).getTeamB().score++;
            } else if (playedGames.get(position).possibleResults.peek().equals(2)) {
                playedGames.get(position).getTeamB().score += 3;
            } else {
                playedGames.get(position).getTeamA().score += 3;
            }
            playedGames.get(position).getTeamA().games++;
            playedGames.get(position).getTeamB().games++;
            playedGames.get(position).possibleResults.pop();
            ++position;
        }else {
            playedGames.get(position).getTeamA().score += 3;
            playedGames.get(position).getTeamA().games++;
            playedGames.get(position).getTeamB().games++;
            playedGames.get(position).possibleResults.empty();
            playedGames.get(position).newStack();
            getPreviousStack(--position);
        }
    }

    public void printResult(){
        ArrayList<Integer> finalResults = getResults();
        for (int i = finalResults.size(); i < 0; i--){
            //int result = finalResults.get(i);
//            if(finalResults.get(i).equals(0)){
//                Visa.out.println(finalResults.get(i) + "\n");
//            }
            System.out.println( finalResults.get(i) + "\n");
        }
    }
}
