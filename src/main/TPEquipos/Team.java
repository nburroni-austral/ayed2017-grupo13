package main.TPEquipos;

/**
 * Created by Florencia on 4/17/17.
 */
public class Team {
    public int score = 0;
    public int games;
    public String name;

    public Team(String name, int score) {
        this.score = score;
        this.name = name;
    }

    public int getScore() {
        return score;
    }

    public int getGames() {
        return games;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public void setGames(int games) {
        this.games = games;
    }

    public boolean wonEverything(){
        if(score == games*3){
            return true;
        }
        return false;
    }

    public boolean lostEverything(){
        if(score == 0){
            return true;
        }
        return false;
    }
}
