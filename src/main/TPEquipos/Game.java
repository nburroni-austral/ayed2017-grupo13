package main.TPEquipos;

import main.stacks.DynamicStack;

/**
 * Created by Florencia on 4/17/17.
 */
public class Game {
    private Team teamA;
    private Team teamB;
    public DynamicStack<Integer> possibleResults;

    public Game(Team teamA, Team teamB) {
        this.teamA = teamA;
        this.teamB = teamB;
        newStack();
    }

    public void getGames(){
        teamA.games++;
        teamB.games++;
    }

    public Team getTeamA() {
        return teamA;
    }

    public Team getTeamB() {
        return teamB;
    }

    public void newStack(){
        possibleResults = new DynamicStack<Integer>();
        possibleResults.push(1);
        possibleResults.push(2);
        possibleResults.push(0);
    }
}
