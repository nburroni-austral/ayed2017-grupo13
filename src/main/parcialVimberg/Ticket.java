package main.parcialVimberg;

import main.StaticStack;

/**
 * Created by Florencia on 4/26/17.
 */
public class Ticket {
    private int value;
    private String type;
    private StaticStack<Integer> usesLeft;

    public Ticket(int value, String type, int capacity) {
        this.value = value;
        this.type = type;
        usesLeft = new StaticStack<>(capacity);
    }

    public String getType() {
        return type;
    }

    public StaticStack<Integer> getUsesLeft() {
        return usesLeft;
    }
}
