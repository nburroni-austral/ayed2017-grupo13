package main.parcialVimberg;

import main.StaticStack;

/**
 * @author Maria Florencia Vimberg
 * class to simulate a fast pass ticket which extends from a ticket class and has a static stack of valid uses.
 */
public class FastPassTicket extends Ticket {
    private StaticStack<Integer> usesLeft = new StaticStack<>(8);

    public FastPassTicket(){
        super(60, "Fast pass", 8);
    }

    public StaticStack<Integer> getUsesLeft() {
        return usesLeft;
    }
}
