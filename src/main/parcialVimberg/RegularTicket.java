package main.parcialVimberg;

/**
 * @author Maria Florencia Vimberg
 * class to simulate a regular ticket, which extends from a ticket and has an unlimited amount of uses.
 */
public class RegularTicket extends Ticket {
    private String type = "";

    public RegularTicket() {
        super(35, "Regular", 0);
    }
}
