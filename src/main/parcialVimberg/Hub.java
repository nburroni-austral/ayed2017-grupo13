package main.parcialVimberg;

import main.StaticList;

import java.util.Random;

/**
 * @author Maria Florencia Vimberg
 * class to simulate the hub, where clients wait untill they get to another game or choose to leave, this decision is
 * taken by the method chooseNextActivity()
 */
public class Hub {
    private ThemePark themePark;
    private StaticList<Client> clientsAtHub = new StaticList<>();

    public Hub(ThemePark themePark) {
        this.themePark = themePark;
    }

    public void chooseNextActivity(int time){
        Client client = clientsAtHub.getActual();
        Random random = new Random();
        int probability = random.nextInt(10)+1;
        if(probability > 1 && probability <= 6){
            Attraction attraction = themePark.getMostPopularAttraction();
            client.goToAttraction(attraction);
        } else if(probability > 6 && probability <= 8){
            client.leavePark(time);
        }
    }

    public void enterClient(Client client){
        clientsAtHub.insertNext(client);
    }
}
