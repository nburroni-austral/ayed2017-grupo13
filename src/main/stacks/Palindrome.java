package main.stacks;

import main.DynamicQueue;

/**
 * Created by Tincho on 10-Apr-17.
 */
public class Palindrome {
    String word = "";

    public Palindrome() {
        word = Scanner.getString("Ingrese una palabra: ");
    }


    private Boolean answer(){
        DynamicStack<Character> checkerPila = new DynamicStack<Character>();
        DynamicQueue<Character> checkerCola = new DynamicQueue<Character>();
        boolean check = false;
        for (int i = 0; i<word.length();i++){
            checkerPila.push(word.charAt(i));
            checkerCola.enqueue(word.charAt(i));
        }
        boolean cond = true;
        while (cond){
            if (checkerPila.peek() != checkerCola.dequeue()){
                cond = false;
                check = false;
            }else{
                if (checkerPila.size == 1){
                    check = checkerPila.peek() == checkerCola.dequeue();
                    cond = false;
                }else {
                    check = true;
                }
            }
        }
        return check;
    }
}
