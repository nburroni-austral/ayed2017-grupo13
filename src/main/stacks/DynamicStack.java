package main.stacks;

import struct.istruct.Stack;

/**
 * Created by Florencia on 3/25/17.
 */
public class DynamicStack<T> implements Stack<T>{

    Nodo n;
    int size = 0;

    private class Nodo<T> {
        private T data;
        private Nodo<T> next;

        public Nodo(T o) {
            data = o;
        }
    }

    public DynamicStack(){
        n = null;
    }

    @Override
    public void push(T o) {
        Nodo aux = new Nodo(o);
        if (n==null) {
            aux.next = null;
            n = aux;

        }
        else
        {
            aux.next = n;
            n = aux;
        }
        size++;
    }

    @Override
    public void pop() {
        if (n != null) {
            n = n.next;
            size--;
        }
    }

    @Override
    public T peek() {
        if (n != null)
        {
            T top =  (T)n.data;

            return top;
        }
        return null;
    }

    @Override
    public boolean isEmpty() {
        if(size == 0){
            return true;
        }
        return false;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public void empty() {
        size = 0;
    }
}
