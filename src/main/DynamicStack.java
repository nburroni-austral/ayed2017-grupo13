package main;

import struct.istruct.Stack;

/**
 * Created by Florencia on 3/25/17.
 */
public class DynamicStack<T> implements Stack<T>{

    Nodo first;
    int size = 0;

    private class Nodo<T> {
        private T data;
        private Nodo<T> next;

        public Nodo(T o) {
            data = o;
        }
    }

    public DynamicStack(){
        first = null;
    }

    @Override
    public void push(T o) {
        Nodo aux = new Nodo(o);
        if (first == null) {
            first = aux;

        }
        else
        {
            aux.next = first;
            first = aux;
        }
        size++;
    }

    @Override
    public void pop() {
        if (first != null) {
            first = first.next;
            size--;
        }
    }

    @Override
    public T peek() {
        if (first != null)
        {
            T top =  (T) first.data;

            return top;
        }
        return null;
    }

    @Override
    public boolean isEmpty() {
        if(size == 0){
            return true;
        }
        return false;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public void empty() {
        size = 0;
    }
}
