package main.tree234;
/**
 * Created by IntelliJ IDEA.
 * User: guest
 * Date: 02-jun-2006
 * Time: 14:15:51
 * To change this template use File | Settings | File Templates.
 */
public abstract class Node <T extends Comparable<T>> {
    private Node<T> father;
    public int type;
    int x;
    int y;
    public abstract Node<T> search(T c);
    public abstract boolean isLeaf();
    public abstract Node<T> insert(T object);
    public abstract void setChild(T o, Node<T> child);
    public abstract void print();
    public Node<T> getFather() {
        return father;
    }
    public abstract Object[] getData();
    public void setFather(Node<T> father) {
        this.father = father;
    }

}
