package main.TPRandomAccessFile;

/**
 * Created by Florencia on 6/23/17.
 */
public class Student {
    private String name; //10
    private char gender; //1
    private int enrollmentId; //8
    private boolean available; //1

    public Student(String name, char gender, int enrollmentId) {
        this.name = name;
        this.gender = gender;
        this.enrollmentId = enrollmentId;
        available = true;
    }

    public String getName() {
        return name;
    }

    public char getGender() {
        return gender;
    }

    public int getEnrollmentId() {
        return enrollmentId;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }
}
