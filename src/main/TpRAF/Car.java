package main.TpRAF;

/**
 * Created by Tincho on 26-Jun-17.
 */
public class Car {
        private int code;
        private double price;
        private boolean available;
        private char type;
        private boolean active;

    public Car(int codigo, double precio, char tipo) {
        this.code = codigo;
        this.price = precio;
        this.type = tipo;
        available = true;
        active = true;
    }

    public Car(int code, double price, boolean available, char type, boolean active) {
        this.code = code;
        this.price = price;
        this.type = type;
        this.available = available;
        this.active = active;
    }

    public Car(){
        code = 0;
        price = 0;
        available = false;
        type = 'N';
        active = false;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    public void setType(char type) {
        this.type = type;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public int getCode() {
        return code;
    }

    public double getPrice() {
        return price;
    }

    public boolean isAvailable() {
        return available;
    }

    public char getType() {
        return type;
    }

    public boolean isActive() {
        return active;
    }

    @Override
    public String toString() {
        return "-----\nCar: \n" +
                "Code: " + code +
                "\nPrice: " + price +
                "\nAvailable:" + available +
                "\nType:" + type +
                "\nActive:" + active +
                "\n------"+"\n";
    }

    public boolean isFull(){
        if (type == 'F' || type == 'f'){
            return true;
        }else {
            return false;
        }
    }

    public boolean isBase(){
        if (type == 'B' || type == 'b'){
            return true;
        }else {
            return false;
        }
    }

}
