package main.knightTour;

import main.stacks.DynamicStack;

import java.util.ArrayList;

/**
 * Created by Florencia on 3/31/17.
 */
public class Knight {
    private Spot currentPosition;
    private ArrayList<Spot> posibleSpots = new ArrayList<Spot>(8);
    private ArrayList<Spot> usedSpots = new ArrayList<Spot>();
    private int counter = 0;
    DynamicStack<Spot> possibleMoves = new DynamicStack<>();

    public Knight() {
        currentPosition = new Spot(0,0);
        posibleSpots = calculatePosibleSpots();
    }

    private DynamicStack<Spot> calculateNextMove(){
        DynamicStack<Spot> moves = createStack();
        usedSpots.add(currentPosition);
        for (int i = 0; i < posibleSpots.size(); i++) {
            Spot nextSpot = currentPosition.sum(posibleSpots.get(i));
            if (nextSpot.inBounds() && !isUsed(nextSpot)) {
                moves.push(nextSpot);
                //Visa.out.println(nextSpot.getRow() + "    " +nextSpot.getColumn());
            }
        }
        currentPosition = moves.peek();

        return moves;
    }

    private DynamicStack<Spot> calculateNextMoveUltimo(){
        DynamicStack<Spot> moves = createStack();
        usedSpots.add(currentPosition);
        for (int i = 0; i < posibleSpots.size(); i++) {
            Spot nextSpot = currentPosition.sum(posibleSpots.get(i));
            if (nextSpot.inBounds() && !isUsed(nextSpot)) {
                moves.push(nextSpot);
            }
        }
        return moves;
    }


    public void nextMoves(){
        DynamicStack<Spot> firstMoves = calculateNextMove();
        while (firstMoves.size() >= 1){
            DynamicStack<Spot> secondMoves = calculateNextMove();
            //Visa.out.println("Entro en: " + firstMoves.peek().getRow()+", "+ firstMoves.peek().getColumn() );
            while (secondMoves.size() >= 1){
                DynamicStack<Spot> thirdMoves = calculateNextMove();
                //Visa.out.println("Entro en: " + secondMoves.peek().getRow()+", "+ secondMoves.peek().getColumn() );
                while (thirdMoves.size() >= 1){
                    DynamicStack<Spot> fourthMoves = calculateNextMoveUltimo();
                    int size = fourthMoves.size();
                    for (int i = 0; i <size ; i++) {
                        //Visa.out.println("Entro en: " + thirdMoves.peek().getRow()+", "+ thirdMoves.peek().getColumn() );
                        //Visa.out.println("Posible moves: " + fourthMoves.peek().getRow()+", "+ fourthMoves.peek().getColumn());
                        fourthMoves.pop();
                    }
                    thirdMoves.pop();
                    currentPosition = thirdMoves.peek();
                }
                secondMoves.pop();
                currentPosition = secondMoves.peek();
            }
            firstMoves.pop();
            currentPosition = firstMoves.peek();
        }
    }

    public ArrayList<Spot> actionListener(){
        DynamicStack<Spot> firstMoves = new DynamicStack<Spot>();
        DynamicStack<Spot> secondMoves = new DynamicStack<Spot>();
        DynamicStack<Spot> thirdMoves = new DynamicStack<Spot>();
        DynamicStack<Spot> fourthMoves = new DynamicStack<Spot>();
        ArrayList<Spot> spots = new ArrayList<Spot>(3);
        if (counter == 0){
            firstMoves = calculateNextMoveUltimo();
            spots.add(0,new Spot(0,0));
            possibleMoves = firstMoves;
            counter++;
        }else if (counter == 1){
            firstMoves = calculateNextMove();
            secondMoves = calculateNextMoveUltimo();
            spots.add(1,firstMoves.peek());
            possibleMoves = secondMoves;
            counter++;
        }else if (counter == 2){
            secondMoves = calculateNextMove();
            thirdMoves = calculateNextMoveUltimo();
            spots.add(2,secondMoves.peek());
            possibleMoves = thirdMoves;
            counter++;
        }else if (counter == 3){
            thirdMoves = calculateNextMove();
            fourthMoves = calculateNextMoveUltimo();
            spots.add(3,thirdMoves.peek());
            possibleMoves = fourthMoves;
            counter++;
        }else if (counter == 4){
            if (thirdMoves.isEmpty()){
                if (secondMoves.isEmpty()){
                    firstMoves.pop();
                    counter = 1;
                }else {
                    secondMoves.pop();
                    counter = 2;
                }
            }else {
                thirdMoves.pop();
                counter--;
            }
        }
        return spots;
    }

    public DynamicStack<Spot> createStack(){
        return new DynamicStack<Spot>();
    }

    private ArrayList<Spot> calculatePosibleSpots(){
        Spot a = new Spot(1,2);
        Spot b = new Spot(1,-2);
        Spot c = new Spot(-1,2);
        Spot d = new Spot(-1,-2);

        Spot e = new Spot(2,1);
        Spot f = new Spot(2,-1);
        Spot g = new Spot(-2,1);
        Spot h = new Spot(-2,-1);

        posibleSpots.add(a);
        posibleSpots.add(b);
        posibleSpots.add(c);
        posibleSpots.add(d);
        posibleSpots.add(e);
        posibleSpots.add(f);
        posibleSpots.add(g);
        posibleSpots.add(h);

        return posibleSpots;
    }

    public Spot getCurrentPosition() {
        return currentPosition;
    }

    private boolean isUsed(Spot spot){
        for (int i = 0; i < usedSpots.size(); i++) {
            if (usedSpots.get(i).equals(spot)){
                return true;
            }
        }
        return false;
    }
}
