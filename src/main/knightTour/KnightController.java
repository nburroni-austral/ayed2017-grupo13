package main.knightTour;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Florencia on 4/5/17.
 */
public class KnightController {
    private Knight knight;
    private KnightView chessView;

    public KnightController(){
        knight = new Knight();
        chessView = new KnightView(new NextMoveButton());
    }

    public class NextMoveButton implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            //knight.nextMoves();
            System.out.println("Chess");
        }
    }
}
