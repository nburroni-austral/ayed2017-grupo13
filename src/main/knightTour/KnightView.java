package main.knightTour;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

/**
 * Created by Florencia on 4/5/17.
 */
public class KnightView extends JFrame{
    private JPanel panel = new JPanel();
    JPanel boardPanel;
    JButton nextMove = new JButton("Next move");

    public KnightView(ActionListener nextMoveButton) {
        super("ChessBoard");
        setSize(600,600);
        setResizable(false);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        Board board = new Board();
        boardPanel = board.createBoard();

        nextMove.setSize(50,50);
        nextMove.addActionListener(nextMoveButton);

        add(boardPanel);
        panel.setSize(500,500);
        //add(panel);
        add(BorderLayout.NORTH,panel);
        add(BorderLayout.SOUTH,nextMove);

        setVisible(true);
    }

    public JPanel createBoard(){
        JLabel[][] squares = new JLabel[8][8];
        JPanel auxPanel = new JPanel();
        auxPanel.setSize(500, 500);
        auxPanel.setLayout(new GridLayout(8, 8));

        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                squares[i][j] = new JLabel();
    
                if ((i + j) % 2 == 0) {
                    squares[i][j].setBackground(Color.black);
                } else {
                    squares[i][j].setBackground(Color.white);
                }
                auxPanel.add(squares[i][j]);
            }
        }

        auxPanel.setVisible(true);
        return auxPanel;
    }
}
