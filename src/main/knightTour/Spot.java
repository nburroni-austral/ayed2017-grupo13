package main.knightTour;

/**
 * Created by Florencia on 3/31/17.
 */
public class Spot {
    private int row;
    private int column;
    private boolean canBeUsed = true;

    public Spot(int row, int column) {
        this.row = row;
        this.column = column;
    }

    public int getRow() {
        return row;
    }

    public int getColumn() {
        return column;
    }

    public boolean canBeUsed() {
        return canBeUsed;
    }

    public void setCanBeUsed(boolean canBeUsed) {
        this.canBeUsed = canBeUsed;
    }

    public boolean getCanBeUsed() {
        return canBeUsed;
    }

    public Spot sum(Spot newSpot){
        return new Spot(this.getRow()+newSpot.getRow(), this.getColumn()+newSpot.getColumn());
    }

    boolean inBounds(){
        if(row >= 0 && row < 8 && column >= 0 && column < 8){
            return true;
        }
        return false;
    }

    public boolean equals(Spot spot){
        if (this.row==spot.getRow() && this.column==spot.column){
            return true;
        }
        return false;

    }
}
