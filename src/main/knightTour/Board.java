package main.knightTour;

import javax.swing.*;
import java.awt.*;

/**
 * Created by Florencia on 4/1/17.
 */
public class Board {
    JPanel panel;
    JLabel squares[][] = new JLabel[8][8];

    public Board(){
    }

    public JPanel createBoard(){
        panel = new JPanel();

        panel.setSize(500, 500);
        panel.setLayout(new GridLayout(8, 8));

        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                squares[i][j] = new JLabel();

                if ((i + j) % 2 == 0) {
                    squares[i][j].setBackground(Color.black);
                } else {
                    squares[i][j].setBackground(Color.white);
                }
                panel.add(squares[i][j]);
            }
        }
        panel.setVisible(true);
        return panel;
    }

    public static void main(String[] args) {
        new Board();
    }
}


