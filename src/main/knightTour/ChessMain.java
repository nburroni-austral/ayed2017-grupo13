package main.knightTour;

import javax.swing.*;
import java.awt.*;

/**
 * Created by Florencia on 4/1/17.
 */
public class ChessMain extends JFrame {
    public static void main(String[] args) {
        new ChessMain();
    }

    JPanel chessBoard;
    JButton solve;
    JLabel students;
    JPanel squares[][];

    public ChessMain() {
        chessBoard = new JPanel();
        solve = new JButton("Solve");
        students = new JLabel("Martin Elissamburu \t Florencia main.trees.Vimberg");
        squares = new JPanel[8][8];

        GridLayout layout = new GridLayout();
        layout.setRows(2);
        layout.setColumns(1);
        layout.setVgap(100);

        chessBoard.setLayout(new GridLayout(8, 8));

        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                squares[i][j] = new JPanel();

                if ((i + j) % 2 == 0) {
                    squares[i][j].setBackground(Color.white);
                } else {
                    squares[i][j].setBackground(Color.black);
                }
                chessBoard.add(squares[i][j]);
            }
        }

        solve.setSize(100, 150);
        solve.setVisible(true);

        JPanel panel = new JPanel();

        setTitle("Chess Game");
        setSize(500, 500);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
        setResizable(false);

        panel.setBackground(Color.pink);

        chessBoard.setSize(1000,1000);
        panel.add(chessBoard);
        panel.add(Box.createRigidArea(new Dimension(500,50)));
        panel.add(solve);
        panel.add(Box.createRigidArea(new Dimension(500,50)));
        panel.add(students);

        setContentPane(panel);
        setVisible(true);
    }
}
