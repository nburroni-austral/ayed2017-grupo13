package main.sortedList;

/**
 * Created by Florencia on 5/15/17.
 */
public class Bus implements Comparable<Bus> {
    private int line;
    private long intern;
    private int seats;
    private boolean disabled;

    public Bus(int line, long intern, int seats, boolean disabled) {
        this.line = line;
        this.intern = intern;
        this.seats = seats;
        this.disabled = disabled;
    }

    public Bus(){
        line = Scanner.getInt("Bus line: ");
        intern = Scanner.getLong("Intern number: ");
        seats = Scanner.getInt("Amount of seats: ");
        disabled = setDisabled();
    }

    public int getLine() {
        return line;
    }

    public long getIntern() {
        return intern;
    }

    public int getSeats() {
        return seats;
    }

    public boolean isDisabled() {
        return disabled;
    }

    @Override
    public int compareTo(Bus o) {
        if( this.line == o.getLine()){
            return 0;
        } else if(this.line < o.getLine()){
            return -1;
        } else {
            return 1;
        }
    }

    public boolean setDisabled() {
        String disability = Scanner.getString("Is bus for disabled people? Y/N: ");
        if (disability.equals("Y")){
            return true;
        }
        return false;
    }
}
