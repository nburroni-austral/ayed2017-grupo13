package main.sortedList;

/**
 * Created by Florencia on 5/16/17.
 */
public class SecretariaTransporteMain {

    public static void main(String[] args) {
        SecretariaTransporte secretaria = new SecretariaTransporte();
        System.out.println("Choose from \n 1. add a bus \n 2. delete a bus \n 3. get an inform \n" +
                " 4. get amount of buses for disabled \n 5. get amount of buses above 27 seats \n" +
                " 6. save the list of bueses \n 7. get list from a file \n 8. close");
        int choice = Scanner.getInt("Choose an option: ");

        while(choice != 8){
            switch (choice){
                case 1:
                    secretaria.addBus();
                    break;

                case 2:
                    secretaria.deleteBus();
                    break;

                case 3:
                    secretaria.getInform();
                    break;

                case 4:
                    secretaria.disabledAmount();
                    break;

                case 5:
                    secretaria.BusAboveXSeats(27);
                    break;

//            case 6:
//                secretaria.saveList();
//                break;
//
//            case 7:
//                secretaria.getList();
//                break;
            }
        }

    }
}
