package main.sortedList;

/**
 * Created by Florencia on 5/16/17.
 */
public class SecretariaTransporte {

    DynamicSortedList<Bus> buses = new DynamicSortedList<Bus>();

    public void addBus() {
        Bus newBus = new Bus();
        buses.insert(newBus);
    }

    public void deleteBus() {
        long internBus = Scanner.getLong("Intern of bus you want to delete: ");
        for (int i = 0; i < buses.size(); i++) {
            buses.goTo(i);
            if(buses.getActual().getIntern() == internBus){
               // buses.remove(buses.getActual());
            }
        }
    }

    public void getInform() {
        for (int i = 0; i < buses.size(); i++) {
            buses.goTo(i);
            System.out.println("Bus line: " + buses.getActual().getLine());
            System.out.println("Bus intern number: " + buses.getActual().getIntern());
        }
    }

    public void disabledAmount() {
        int disabledAmount = 0;
        buses.goTo(0);
        int currentLine = buses.getActual().getLine();
        for (int i = 0; i < buses.size(); i++) {
            buses.goTo(i);
            if(buses.getActual().getLine() == currentLine){
                if(buses.getActual().isDisabled()){
                    disabledAmount++;
                }
            } else {
                System.out.println("Disabled buses at line "+ currentLine +": "+ +disabledAmount);
                currentLine = buses.getActual().getLine();
                disabledAmount = 0;
            }
        }
    }

    public void BusAboveXSeats(int seats) {
        int count = 0;
        buses.goTo(0);
        int currentLine = buses.getActual().getLine();
        for (int i = 0; i < buses.size(); i++) {
            buses.goTo(i);
            if(buses.getActual().getLine() == currentLine){
                if(buses.getActual().getSeats() > seats){
                    count++;
                }
            } else {
                System.out.println("Buses above "+ seats + "in line "+ currentLine +": "+ +count);
                currentLine = buses.getActual().getLine();
                count = 0;
            }
        }
    }
}
