package main;

/**
 * Created by Florencia on 3/25/17.
 */
public class Car {
    private String patent;
    private String brand;
    private String model;
    private String color;

    public Car(String patent, String brand, String model, String color){
        this.patent = patent;
        this.brand = brand;
        this.model = model;
        this.color = color;
    }

    public void changeColor(String newColor){
        color = newColor;
    }

    public String getPatent() {
        return patent;
    }

    public String getBrand() {
        return brand;
    }

    public String getModel() {
        return model;
    }

    public String getColor() {
        return color;
    }
}
