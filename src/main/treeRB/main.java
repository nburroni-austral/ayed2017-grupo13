package main.treeRB;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Scanner;

/**
 * Created by Florencia on 5/31/17.
 */
public class main {
    public static void main(String[] args) throws IOException {
        TreeRB treeRB = new TreeRB();

        int dni = 1;
        while (dni != 0){
            System.out.println("Please enter dni or 0 to stop: ");
            Scanner scanner = new Scanner(System.in);
            dni = scanner.nextInt();
            if (dni != 0){
                treeRB.insert(dni);
            }
        }

//        treeRB.insert(5);
//        treeRB.insert(6);
//        treeRB.insert(9);
//        treeRB.insert(4);
//        treeRB.insert(3);

        treeRBImage printTree = new treeRBImage();
        int height = 1000;
        int width = 1000;

        BufferedImage image = new BufferedImage(width,height,BufferedImage.TYPE_INT_RGB);
        Graphics2D graphic = image.createGraphics();

        graphic.setColor(Color.white);
        graphic.fillRect(0, 0, width, height);

        printTree.printTree(470,30,treeRB.root,1,graphic);

        File file = new File("myimage.png");
        ImageIO.write(image, "png", file);
        System.out.println("tree finished");
    }


}
