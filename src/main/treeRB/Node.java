package main.treeRB;

import main.trees.Vimberg.DoubleNode;

/**
 * Created by Florencia on 5/22/17.
 */
public class Node<T extends Comparable<T>> {
    T data;
    Node<T> left;
    Node<T> right;
    private Node<T> father;
    private boolean isBlack = false;


    public boolean isLeaf(){
        if(this.left == null && this.right == null){
            return true;
        } else {
            return false;
        }
    }

    public void setChild(Node<T> o, Node<T> child){
        int comparedWithData1 = child.data.compareTo(o.data);
        if(comparedWithData1>0){
            o.setRight(child);
        } else {
            o.setLeft(child);
        }
    }
    //public void print();
    public Node<T> getFather() {
        if(father == null){
            return this;
        }
        return father;
    }
    //public Object[] getData();
    public void setFather(Node<T> father) {
        this.father = father;
    }

    public Node(T o){
        data = o;
    }
    public Node(T o, Node<T> left, Node<T> right){
        data = o;
        this.right = right;
        this.left = left;
    }

    public Node<T> getRight() {
        return right;
    }

    public Node<T> getLeft() {
        return left;
    }

    public T getData() {
        return data;
    }

    public void setRight(Node<T> right) {
        this.right = right;
    }

    public void setLeft(Node<T> left) {
        this.left = left;
    }

    public boolean isBlack() {
        return isBlack;
    }

    public void setRed() {
        isBlack = false;
    }

    public void setBlack(){
        isBlack = true;
    }

    // get's the father of the node inserted and return true if left child and false if right child
    public boolean isLeft(){
        if (data.compareTo(father.data) < 0){
            return true;
        }else {
            return false;
        }
    }

    public boolean isExterior(){
        if (( this.isLeft() && father.isLeft() ) ||
                (!this.isLeft() && !father.isLeft() )){
            return true;
        } else {
            return false;
        }
    }


    //returns true if the uncle is black or null -- brother
    public boolean brotherIsBlackOrNull(){
        if(this.father == null){
            return true;
        }
        else{
            if(this.isLeft() ){
                if(this.father.right == null || this.father.right.isBlack()){
                    return true;
                }
            }
            else{
                if(this.father.left == null || this.father.left.isBlack()){
                    return true;
                }
            }
            return false;
        }
    }

    //gets the brother from a father -> an uncle -- getBrother()
    public Node<T> getUncle(){
        Node<T> grandFather = this.father;
        if (this.isLeft()){
            return grandFather.getRight();
        }else {
            return grandFather.getLeft();
        }
    }
}
