package main.treeRB;

/**
 * Created by Florencia on 5/22/17.
 */
class TreeRB<T extends Comparable<T>> {
    public Node<T> root;

    public TreeRB(){
        root = null;
    }

    public Node<T> search(Node<T> c){
        return search(root, c);
    }

    public void insert(T data){
        Node<T> newNode = new Node<T>(data);
        if (isEmpty()){
            root = newNode;
            newNode.setBlack();
        }
        else{
            Node<T> father = search(newNode);
            father.setChild(father, newNode);
            newNode.setFather(father);
        }
        insert(newNode);
    }

    private void insert(Node<T> newNode){
        Node father = newNode.getFather();
        if (father.isBlack()) {
        }
        else if(newNode == root){
        }
        else if (!father.isBlack() && father.brotherIsBlackOrNull() && newNode.isExterior()) {

            simpleRotation(newNode);
        } else if (!father.isBlack() && father.brotherIsBlackOrNull() && !newNode.isExterior()) {

            doubleRotation(newNode);
        } else {
            case4(newNode);
        }
        root.setBlack();
    }




    private Node<T> search(Node<T> t, Node<T> x){
        if (x.data.compareTo(t.data) == 0){
            return t;
        } else if (x.data.compareTo( t.data)< 0){
            if (t.left == null){
                return t;
            }else {
                return search(t.left, x);
            }
        } else if(x.data == null){
            return x.getFather();
        } else{
            if (t.right == null){
                return t;
            }else{
                return search(t.right, x);
            }
        }
    }

    public void simpleRotation(Node<T> son){
        Node<T> father = son.getFather();
        Node<T> grandFather = father.getFather();
        Node<T> greatGrandFather = grandFather.getFather();

        if(grandFather.data == root.data){
            root = father;
            father.setChild(father, grandFather);
            if (father.isLeft()) {
                grandFather.left = null;
            }else {
                grandFather.right = null;
            }
            grandFather.setFather(father);
            father.setFather(null);
        } else {

            if (grandFather.isLeft()){
                grandFather.left = null;
            }else {
                grandFather.right = null;
            }
            greatGrandFather.setChild(greatGrandFather,father);
            father.setFather(greatGrandFather);
            father.setChild(father,grandFather);
            grandFather.setFather(father);
        }
        grandFather.setRed();
        father.setBlack();
    }

    public void doubleRotation(Node<T> son){
        Node<T> father = son.getFather();
        Node<T> grandFather = father.getFather();
        Node<T> uncle = father.getUncle();
        Node<T> greatGrandFather = grandFather.getFather();

        if (greatGrandFather.data != grandFather.data) {
            greatGrandFather.setChild(greatGrandFather, son);
            son.setFather(greatGrandFather);
            son.setChild(son, father);
            father.setFather(son);
            son.setChild(son, grandFather);
            grandFather.setFather(son);
        }else {
            if (father.isLeft()){
                father.right = null;
                grandFather.left = null;
            }else {
                father.left = null;
                grandFather.right = null;
            }
            root = son;
            son.setFather(null);
            son.setChild(son,father);
            son.setChild(son,grandFather);
            grandFather.setFather(son);
            father.setFather(son);
        }
        son.setBlack();
        grandFather.setRed();
    }

    public void case4(Node<T> son) {
        Node<T> father = son.getFather();
        Node<T> grandFather = father.getFather();
        Node<T> uncle = father.getUncle();
        father.setBlack();
        uncle.setBlack();
        grandFather.setRed();

        insert(grandFather);
    }

    public boolean isEmpty(){
        return (root == null);
    }
}
