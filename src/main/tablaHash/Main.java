package main.tablaHash;

import main.archivos.Scanner;

/**
 * Created by Florencia on 6/25/17.
 */
public class Main {
    static TablaHash dictionary = new TablaHash();
    public static void main(String[] args) {
        dictionary.insert("Beer");
  //      dictionary.insert("Bear");
        dictionary.insert("Bearer");
        //dictionary.insert("Bare");

        int choice = 0;
        while (choice != 3){
            choice = Scanner.getInt(" 1. Add a word \n 2. Get similar words \n 3. Exit \n Choose an option: ");
            switch (choice){
                case 1:
                    addWord();
                    break;
                case 2:
                    getSimilarWords();
                    break;
                case 3:
                    break;
                default:
                    System.out.println("Please enter a valid operation.");
            }
        }

    }

    public static void addWord(){
        String newWord = Scanner.getString("Enter the word to add: ");
        dictionary.insert(newWord);
    }

    public static void getSimilarWords(){
        String word = Scanner.getString("Enter word: ");
        dictionary.getSimilar(word);
    }

}
