package main;

import struct.istruct.Stack;

/**
 * Created by Florencia on 3/25/17.
 */
public class StaticStack<T> implements Stack<T> {
    private int top;
    private int size;
    private T[] data;

    public StaticStack(int x){
        top = -1;
        size = x;
        data = (T[]) new Object[size];
    }

    @Override
    public void push(T o) {
        if(top+1 == data.length){
            grow();
        }
        top++;
        data[top] = (T) o;
    }

    @Override
    public void pop() {
        top--;
    }

    @Override
    public T peek() {
        if(!isEmpty()){
            return data[top];
        }
        return null;
    }

    @Override
    public boolean isEmpty() {
        if(top == -1){
            return true;
        }
        return false;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public void empty() {
        top = -1;
        size = 0;
    }

    public void grow(){
        size = size*2;
        T[] aux = (T[]) new Object[size];
        for (int i = 0; i < data.length; i++){
            aux[i] = data[i];
        }
        data = aux;
    }
}
