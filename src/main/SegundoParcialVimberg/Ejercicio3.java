package main.SegundoParcialVimberg;

import java.io.*;

/**
 * Created by Florencia on 6/14/17.
 */
public class Ejercicio3 {
    public void ejercicio3(File file){
        try {
            FileReader fileReader = new FileReader(file);
            BufferedReader bf = new BufferedReader(fileReader);
            String currentLine = bf.readLine();
            int totalLines = 0;
            int amountPassed = 0;
            while (currentLine != null){
                int index = currentLine.charAt(8);
                char uno = '1';
                if(index == uno){
                    amountPassed++;
                }
                totalLines++;

                currentLine = bf.readLine();
            }
            int percentage = (amountPassed * 100) / totalLines;
            System.out.println("Percentage of cars that passed the test: " + percentage + " %");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
