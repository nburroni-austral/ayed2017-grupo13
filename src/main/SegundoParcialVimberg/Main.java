package main.SegundoParcialVimberg;

import main.archivos.Scanner;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Created by Florencia on 6/14/17.
 */
public class Main {
    public static void create(){
        try{
            FileWriter flor = new FileWriter("patentes");
            flor.write("MO123xtP1 \n");
            flor.write("123456781 \n");
            flor.write("SqrxtW232 ");
            flor.close();
        }
        catch(IOException e){
            System.out.println(e.getMessage());
        }
    }

    public static void main(String[] args) throws IOException {
        create();
        File fileChosen = null;
        fileChosen = new File(Scanner.getString("File name: "));
        Ejercicio3 ejercicio3 = new Ejercicio3();
        ejercicio3.ejercicio3(fileChosen);
    }
}
