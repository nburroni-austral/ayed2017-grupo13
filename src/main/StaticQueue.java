package main;

import struct.istruct.Queue;

/**
 * Created by Florencia on 4/5/17.
 */
public class StaticQueue<Q> implements Queue<Q> {
    private int front;
    private int back;
    private int size;
    private int length;
    private Q[] data;

    public StaticQueue(int size){
        front = 0;
        back = 0;
        this.size = size;
        data = (Q[]) new Object[size];
        length = 0;
    }
    @Override
    public void enqueue(Q q){
        if(length != size && back<=(size-1)){
            data[back] = q;
            back++;
            length++;
        } else {
            if(back == size && length < size){
                back = 0;
                data[back] = q;
                back++;
                length++;
            } else {
                grow();
                data[back] = q;
                back++;
                length++;
            }
        }
    }

    @Override
    public Q dequeue() {
        if(!isEmpty()){
            getNext(front+1);
            length--;
            return data[front];
        }
        throw new RuntimeException("Empty queue");
    }

    @Override
    public boolean isEmpty() {
        if(front == 0 && back == 0){
            return true;
        }
        return false;
    }

    @Override
    public int length() {
        return length;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public void empty() {
        front = 0;
        back = 0;
        //+
    }

    private int getNext(int toCalculate){
        if(toCalculate + 1 >= length){
            return 0;
        }
        return toCalculate+1;
    }

    private void grow(){
        Q[] auxQueue = (Q[]) new Object[size*2];
        int j = 0;
        for(int i = front; i < length; i++){
            auxQueue[j] = data[i];
            j++;
        }
        for(int i = 0; i < back; i++){
            auxQueue[j] = data[i];
            j++;
        }
        data = auxQueue;
        front = 0;
        back = length;
        size = size*2;
    }
}
