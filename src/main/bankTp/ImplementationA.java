package main.bankTp;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by Florencia on 4/11/17.
 */
public class ImplementationA implements Implementation {
    private Cashier cashierA = new Cashier(30,90);
    private Cashier cashierB = new Cashier(30,120);
    private Cashier cashierC = new Cashier(30,150);

    @Override
    public void goWorking(Bank bank){
        for (int time = 0; time <= 18000; time++){
            if(time % 90 == 0){
                ArrayList<Client> arrayClients = enterClients();
                for(int i = 0; i < arrayClients.size(); i++){
                    bank.clientsWaiting.enqueue(arrayClients.get(i));
                    arrayClients.clear();
                }
            }
            for (int i = 0; i < bank.cashiers.size() ; i++) {
                bank.cashiers.get(i).checkOccupied(time);
            }
            setClients(bank, time);
        }
        while (bank.peopleInside()){
            int extraTime = 18001;
            setClients(bank, extraTime);
            extraTime++;
        }
    }

    private ArrayList<Client> enterClients(){
        ArrayList<Client> arrayClients = new ArrayList<>();
        Random random = new Random();
        int numberOfClients = random.nextInt(5) + 1;
        for(int i = 0; i < numberOfClients; i++){
            arrayClients.add(new Client());
        }
        return arrayClients;
    }

    private int amountFreeCashiers(Bank bank){
        int free = 0;
        for (int i = 0; i<bank.cashiers.size(); i++){
            if (!bank.cashiers.get(i).isOccupied()){
                free++;
            }
        }
        return free;
    }

    public void setClients(Bank bank, int time){
        int freeCashiers = amountFreeCashiers(bank);
        if(freeCashiers == 3){
            ArrayList<Cashier> arrayFreeCashiers = new ArrayList<>();
            arrayFreeCashiers.add(cashierA);
            arrayFreeCashiers.add(cashierB);
            arrayFreeCashiers.add(cashierC);
            Random random = new Random();
            int chosenCashier = random.nextInt(arrayFreeCashiers.size());
            arrayFreeCashiers.get(chosenCashier).working(time);
            bank.clientsWaiting.dequeue();
        }else if (freeCashiers == 2){
            for (int i = 0; i < bank.cashiers.size() ; i++) {
                if(!bank.cashiers.get(i).isOccupied()){
                    bank.cashiers.get(i).working(time);
                    bank.clientsWaiting.dequeue();
                }
            }
        } else if(freeCashiers == 1){
            ArrayList<Cashier> arrayFreeCashiers = new ArrayList<>();
            for (int i = 0; i < bank.cashiers.size() ; i++) {
                if(!bank.cashiers.get(i).isOccupied()){
                    arrayFreeCashiers.add(bank.cashiers.get(i));
                }
            }
            Random random = new Random();
            int chosenCashier = random.nextInt(arrayFreeCashiers.size());
            arrayFreeCashiers.get(chosenCashier).working(time);
            bank.clientsWaiting.dequeue();
        }
    }
}
