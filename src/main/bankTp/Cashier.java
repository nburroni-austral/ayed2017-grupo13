package main.bankTp;

import main.DynamicQueue;

import java.util.Random;

/**
 * Created by Florencia on 4/11/17.
 */
public class Cashier {
    private boolean occupied;
    private int timeClientArrives;
    private int timeOccupied;
    public int minTimeOccupied;
    public int maxTimeOccupied;
    DynamicQueue<Client> waitingQueue;


    public Cashier(int start, int end){
        this.minTimeOccupied = start;
        this.maxTimeOccupied = end;
        occupied = false;
        waitingQueue = new DynamicQueue<>();
    }

    public void working(int currentTime){
        timeClientArrives = currentTime;
        Random random = new Random();
        timeOccupied = random.nextInt(maxTimeOccupied) + minTimeOccupied;
        setOccupied(false);
    }

    public void checkOccupied(int time){
        if (timeOccupied == time-timeClientArrives){
            occupied = false;
        }
    }

    public boolean isOccupied() {
        return occupied;
    }

    public void setOccupied(boolean occupied) {
        this.occupied = occupied;
    }
}
