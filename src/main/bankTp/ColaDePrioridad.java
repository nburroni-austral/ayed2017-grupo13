package main.bankTp;

import main.DynamicQueue;

import java.util.EmptyStackException;

/**
 * Created by Tincho on 10-Apr-17.
 */
public class ColaDePrioridad<Q>{
    DynamicQueue<Q>[] arrayPriorityQueue;

    public ColaDePrioridad(int priorityLevels) {
        arrayPriorityQueue = new DynamicQueue[priorityLevels];
        for (int i = 0; i < arrayPriorityQueue.length; i++) {
            arrayPriorityQueue[i] = new DynamicQueue<Q>();
        }
    }

    public void enqueue(Q q, int index) {
        arrayPriorityQueue[index].enqueue(q);
    }

    public Q dequeue() {
        for (int i =0; i<arrayPriorityQueue.length; i++){
            if (!arrayPriorityQueue[i].isEmpty()){
                return arrayPriorityQueue[i].dequeue();
            }
        }throw new EmptyStackException();
    }

    public boolean isEmpty() {
        boolean empty = true;
        for (int i = 0; i < arrayPriorityQueue.length; i++) {
            if (!arrayPriorityQueue[i].isEmpty()){
                empty = false;
            }
        }
        return empty;
    }

    public int size() {
        int size = 0;
        for (int i = 0; i < arrayPriorityQueue.length ; i++) {
            size += arrayPriorityQueue[i].size;
        }
        return size;
    }

    public void empty() {
        for (int i = 0; i < arrayPriorityQueue.length; i++) {
            arrayPriorityQueue[i] = null;
        }
    }
}
