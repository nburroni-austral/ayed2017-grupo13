package main.bankTp;

import main.DynamicQueue;

import java.util.ArrayList;

/**
 * Created by Florencia on 4/11/17.
 */
public class Bank {
    private Cashier cashierA = new Cashier(30,90);
    private Cashier cashierB = new Cashier(30,120);
    private Cashier cashierC = new Cashier(30,150);
    public ArrayList<Cashier> cashiers = new ArrayList<>();
    public DynamicQueue<Client> clientsWaiting = new DynamicQueue<>();

    public Bank(Implementation implementation){
        cashiers.add(cashierA);
        cashiers.add(cashierB);
        cashiers.add(cashierC);

        implementation.goWorking(this);
    }

    public boolean peopleInside(){
        if(!clientsWaiting.isEmpty() && !cashierA.isOccupied() &&
                !cashierB.isOccupied() && !cashierC.isOccupied()){
            return true;
        }
        return false;
    }

    public boolean peopleInQueue(){
        if(cashierA.waitingQueue.isEmpty() && cashierB.waitingQueue.isEmpty() && cashierC.waitingQueue.isEmpty()){
            return    true;
        }
        return false;
    }
}
