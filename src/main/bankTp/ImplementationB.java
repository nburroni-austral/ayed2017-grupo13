package main.bankTp;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by Florencia on 4/12/17.
 */
public class ImplementationB implements Implementation {

    private Cashier cashierA = new Cashier(30,90);
    private Cashier cashierB = new Cashier(30,120);
    private Cashier cashierC = new Cashier(30,150);

    @Override
    public void goWorking(Bank bank){
        for (int time = 0; time <= 18000; time++){
            if(time % 90 == 0){
                ArrayList<Client> clients = enterClients();
                for (int i = 0; i < clients.size(); i++){
                    setClients(bank, time, clients.get(i));
                }
            }
            for (int i = 0; i < bank.cashiers.size() ; i++) {
                bank.cashiers.get(i).checkOccupied(time);
                if(!bank.cashiers.get(i).isOccupied()){
                    bank.cashiers.get(i).waitingQueue.dequeue();
                }
            }
        }
        while (bank.peopleInside()){
            int extraTime = 18001;
            closingBank(bank,extraTime);
            extraTime++;
        }
    }

    private ArrayList<Client> enterClients(){
        ArrayList<Client> arrayClients = new ArrayList<>();
        Random random = new Random();
        int numberOfClients = random.nextInt(5) + 1;
        for(int i = 0; i < numberOfClients; i++){
            arrayClients.add(new Client());
        }
        return arrayClients;
    }

    private ArrayList<Cashier> getPosibleCashiers(Bank bank){
        ArrayList<Cashier> freeCashiers = new ArrayList<>();
        Cashier smallestSizeCashier = new Cashier(0,0);
        for (int i = 0; i < bank.cashiers.size(); i++) {
            if(bank.cashiers.get(i).waitingQueue.size() == smallestSizeCashier.waitingQueue.size()){
                freeCashiers.add(bank.cashiers.get(i));
            } else if(bank.cashiers.get(i).waitingQueue.size() < smallestSizeCashier.waitingQueue.size()){
                freeCashiers.clear();
                freeCashiers.add(bank.cashiers.get(i));
                smallestSizeCashier = bank.cashiers.get(i);
            }
        }
        return freeCashiers;
    }

    public void setClients(Bank bank, int time, Client client){
        ArrayList<Cashier> possibleCashiers = getPosibleCashiers(bank);
        if( possibleCashiers.size() == 3){
            Random random = new Random();
            int chosenCashier = random.nextInt(possibleCashiers.size());
            possibleCashiers.get(chosenCashier).waitingQueue.enqueue(client);
            possibleCashiers.get(chosenCashier).working(time);
        }else if (possibleCashiers.size() == 2) {
            Random random = new Random();
            int chosenCashier = random.nextInt(possibleCashiers.size());
            possibleCashiers.get(chosenCashier).waitingQueue.enqueue(client);
            possibleCashiers.get(chosenCashier).working(time);
        } else if(possibleCashiers.size() == 1){
            for (int i = 0; i < bank.cashiers.size() ; i++) {
                if(bank.cashiers.get(i) == possibleCashiers.get(0) ){
                    bank.cashiers.get(i).waitingQueue.enqueue(client);
                    bank.cashiers.get(i).working(time);
                }
            }
        }
    }

    public void closingBank(Bank bank, int time){
        for (int i = 0; i < bank.cashiers.size(); i++) {
            bank.cashiers.get(i).checkOccupied(time);
            if (!bank.cashiers.get(i).waitingQueue.isEmpty() && !bank.cashiers.get(i).isOccupied() ){
                bank.cashiers.get(i).working(time);
                bank.cashiers.get(i).waitingQueue.dequeue();
            }
        }
    }
}
