package main.TPCuatrimestral.Ejercicio1;

import main.DynamicList;

/**
 * Created by Florencia on 5/8/17.
 */
public class Cart {
    private float total;
    private DynamicList<Appliance> itemsAtCart;

    public Cart() {
        this.total = 0;
        itemsAtCart = new DynamicList<>();
    }

    public void addAppliance(Appliance appliance){
        itemsAtCart.goTo(0);
        itemsAtCart.insertNext(appliance);
    }

    public void addAppliance(Appliance appliance, int quantity){
        for (int i = 0; i < quantity; i++) {
            itemsAtCart.goTo(i);
            itemsAtCart.insertNext(appliance);
        }
    }

    public Invoice createInvoice(){
        Invoice invoice = new Invoice();
        for (int i = 0; i < itemsAtCart.size(); i++) {
            itemsAtCart.goTo(i);
            invoice.setAmount(itemsAtCart.getActual().getPrice());
        }
        return invoice;
    }
}
