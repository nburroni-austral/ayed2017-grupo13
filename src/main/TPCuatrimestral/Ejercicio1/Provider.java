package main.TPCuatrimestral.Ejercicio1;

/**
 * Created by Florencia on 5/8/17.
 */
public class Provider { //necesario?
    private String name;
    private String description;
    private String address;
    private String city;
    private String telephoneNumber;
    private String webPage;

    public Provider(String name, String description, String address, String city, String telephoneNumber, String webPage) {
        this.name = name;
        this.description = description;
        this.address = address;
        this.city = city;
        this.telephoneNumber = telephoneNumber;
        this.webPage = webPage;
    }

    public Provider() {
    }

    //
}
