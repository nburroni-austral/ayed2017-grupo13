package main.TPCuatrimestral.Ejercicio1;

/**
 * Created by Florencia on 5/8/17.
 */
public class Appliance {
    private String name;
    private String model;
    private float price;
    private Producer producer;
    private Provider provider;
    private double discount;
    private Tag tag;

    public Appliance(String name, String model, float price, Tag tag) {
        this.name = name;
        this.model = model;
        this.price = price;
        producer = new Producer(); //chooseProducer();
        provider = new Provider(); //chooseProvider();
        this.tag = tag;
    }

    public void setDiscount(double discount){
        this.discount = discount;
    }

    public Tag getTag() {
        return tag;
    }

    public float getPrice() {
        return price;
    }
}
