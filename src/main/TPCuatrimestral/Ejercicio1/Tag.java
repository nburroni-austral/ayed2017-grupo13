package main.TPCuatrimestral.Ejercicio1;

/**
 * Created by Florencia on 5/8/17.
 */
public class Tag {
    private long id = 0;
    private String name;

    public Tag(String name) {
        id = id++;
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
