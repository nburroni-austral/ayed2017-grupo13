package main.TPCuatrimestral.Ejercicio1;

import java.util.HashMap;

/**
 * Created by Florencia on 5/8/17.
 */
public class Catalogue {
    private HashMap<Appliance, Long> applianceCatalogue;

    public Catalogue() {
        applianceCatalogue = new HashMap<>();
    }

    public void addAppliance(Appliance appliance, double discount){
        applianceCatalogue.put(appliance, appliance.getTag().getId());
        appliance.setDiscount(discount);
    }
}
