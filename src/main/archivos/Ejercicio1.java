package main.archivos;

import java.io.*;

/**
 * Created by Florencia on 6/12/17.
 */
public class Ejercicio1 {

    public static void create(){
        try{
            FileWriter flor = new FileWriter("flor");
            flor.write("hola.\n");
            flor.write(":)");
            flor.close();
        }
        catch(IOException e){
            System.out.println(e.getMessage());
        }
    }

    public static void main(String[] args) throws IOException {
        create();
        FileReader fileChosen = null;
        try {
            fileChosen = new FileReader(Scanner.getString("File name: "));
        } catch (IOException e) {
            System.out.println("ERROR!");
        }
        int choice = 0;
        choice = Scanner.getInt("Choose 1 for character count or 2 for lines count or 3 to exit: ");
        switch (choice){
            case 1:
                System.out.println(countCharacters(fileChosen));
                break;
            case 2:
                System.out.println(countLines(fileChosen));
                break;
            case 3:
                break;
            default:
                System.out.println("Please enter a valid operation.");
        }
    }

    public static int countCharacters(FileReader fileReader) throws IOException {
        int count = 0;
        try {
            while (fileReader.read()!= -1){
                count++;
            }
        } catch (IOException e) {
            System.out.println("ERROR!");
        }
        return count;
    }

    public static int countLines(FileReader fileReader) {
        int count = 0;
        try{
            BufferedReader br = new BufferedReader(fileReader);
            while (br.readLine() != null){
                count++;
            }
            br.close();
        }
        catch(IOException e) {
            System.out.println("ERROR!");
        }
        return count;
    }
}

