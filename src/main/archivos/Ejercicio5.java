package main.archivos;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Created by Florencia on 6/12/17.
 */
public class Ejercicio5 {
    public static void create(){
        try{
            FileWriter paises = new FileWriter("Paises");
            paises.write("el salvador;4500000;25.85 Billion;\n");
            paises.write("francia;66500000;2.422 Trillion;\n");
            paises.write("alemania;81410000;3.622 Trillion;\n");
            paises.write("reino unido;65400000;2.849 Trillion;\n");
            paises.write("guatemala;16340000;63.41 Billion;\n");
            paises.close();
        }
        catch(IOException e){
            System.out.println(e.getMessage());
        }
    }

    private static void getReport(FileReader fileReader, int population) throws IOException {
        try{
            FileWriter abovePopulation = new FileWriter("Countries above " + population);
            FileWriter belowPopulation = new FileWriter("Countries below " + population);
            BufferedReader bf = new BufferedReader(fileReader);
            String currentLine = bf.readLine();
            while (currentLine != null){
                String[] line = currentLine.split(";");
                int poblation = Integer.parseInt(line[1]);
                if (poblation <= population){
                    belowPopulation.write(currentLine + "\n");
                } else {
                    abovePopulation.write(currentLine + "\n");
                }
                currentLine = bf.readLine();
            }
            belowPopulation.close();
            abovePopulation.close();
        }
        catch(IOException e){
            System.out.println(e.getMessage());
        }
    }

    private static void getCountryAndGdp(FileReader fileReader, int population) {
        try{
            FileWriter abovePopulation = new FileWriter("Countries above " + population);
            FileWriter belowPopulation = new FileWriter("Countries below " + population);
            BufferedReader bf = new BufferedReader(fileReader);
            String currentLine = bf.readLine();
            while (currentLine != null){
                String[] line = currentLine.split(";");
                int poblation = Integer.parseInt(line[1]);
                String lineString = line[0] + ";" + line[2] + ";";
                if (poblation <= population){
                    belowPopulation.write(lineString + "\n");
                } else {
                    abovePopulation.write(lineString + "\n");
                }
                currentLine = bf.readLine();
            }
            belowPopulation.close();
            abovePopulation.close();
        }
        catch(IOException e){
            System.out.println(e.getMessage());
        }
    }

    private static void getCountryAndPopulation(FileReader fileReader, int population) {
        try{
            FileWriter abovePopulation = new FileWriter("Countries above " + population);
            FileWriter belowPopulation = new FileWriter("Countries below " + population);
            BufferedReader bf = new BufferedReader(fileReader);
            String currentLine = bf.readLine();
            while (currentLine != null){
                String[] line = currentLine.split(";");
                int poblation = Integer.parseInt(line[1]);
                if (poblation <= population){
                    belowPopulation.write(line[0] + line[1] + "\n");
                } else {
                    abovePopulation.write(line[0] + line[1] + "\n");
                }
                currentLine = bf.readLine();
            }
            belowPopulation.close();
            abovePopulation.close();
        }
        catch(IOException e){
            System.out.println(e.getMessage());
        }
    }

    public static void main(String[] args) throws IOException {
        create();
        FileReader fileChosen = null;
        try {
            fileChosen = new FileReader(Scanner.getString("File name: "));
        } catch (IOException e) {
            System.out.println("ERROR!");
        }
        int population = Scanner.getInt("Enter population limit: ");
        int option = Scanner.getInt("Enter 1 if want only country and gdp or 2 if want only country and" +
                " population or 3 for all or 4 exit: ");
        switch (option){
            case 1:
                getCountryAndGdp(fileChosen,population);
                break;
            case 2:
                getCountryAndPopulation(fileChosen,population);
                break;
            case 3:
                getReport(fileChosen,population);
                break;
            case 4:
                break;
            default:
                System.out.println("Please enter valid operation. \n");
                break;
        }
    }
}
