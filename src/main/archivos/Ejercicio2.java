package main.archivos;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Created by Florencia on 6/12/17.
 */
public class Ejercicio2 {
    public static int amountAppearances(FileReader fileReader) throws IOException {
        char character = Scanner.getChar("Choose a character to look amount of repetition: ");
        int count = 0;
        try {
            int value = fileReader.read();
            while (value != -1) {
                if (character == (char)value) {
                    count++;
                }

                value = fileReader.read();
            }
        } catch (IOException e) {
            System.out.println("ERROR!");
        }
        return count;
    }

    public static void create(){
        try{
            FileWriter flor = new FileWriter("flor");
            flor.write("hooooola.\n");
            flor.write(":)");
            flor.close();
        }
        catch(IOException e){
            System.out.println(e.getMessage());
        }
    }

    public static void main(String[] args) throws IOException {
        create();
        FileReader fileChosen = null;
        try {
            fileChosen = new FileReader(Scanner.getString("File name: "));
        } catch (IOException e) {
            System.out.println("ERROR!");
        }
        System.out.println("Amount of appearances: " + amountAppearances(fileChosen));
    }
}
