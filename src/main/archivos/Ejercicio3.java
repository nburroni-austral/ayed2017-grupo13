package main.archivos;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Created by Florencia on 6/12/17.
 */
public class Ejercicio3 {
    public static void toUpperCase(FileReader fileReader) throws IOException {
        FileWriter resultFile = new FileWriter("New file");
        try{
            BufferedReader br = new BufferedReader(fileReader);
            String currentString = br.readLine();
            while (currentString != null){
                resultFile.write(currentString.toUpperCase() + "\n");

                currentString = br.readLine();
            }
            resultFile.close();
            br.close();
        }
        catch(IOException e) {
            System.out.println("ERROR!");
        }
    }

    public static void toLowerCase(FileReader fileReader) throws IOException {
        FileWriter resultFile = new FileWriter("New file");
        try{
            BufferedReader br = new BufferedReader(fileReader);
            String currentString = br.readLine();
            while (currentString != null){
                resultFile.write(currentString.toLowerCase() + "\n");

                currentString = br.readLine();
            }
            resultFile.close();
            br.close();
        }
        catch(IOException e) {
            System.out.println("ERROR!");
        }
    }

    public static void create(){
        try{
            FileWriter flor = new FileWriter("flor");
            flor.write("hooooola.\n");
            flor.write("FLORCHIS");
            flor.close();
        }
        catch(IOException e){
            System.out.println(e.getMessage());
        }
    }

    public static void main(String[] args) throws IOException {
        create();
        FileReader fileChosen = null;
        try {
            fileChosen = new FileReader(Scanner.getString("File name: "));
        } catch (IOException e) {
            System.out.println("ERROR!");
        }
        int choice = 0;
        choice = Scanner.getInt("Choose 1 to go to upper case or 2 to go to lower case or 3 to exit: ");
        switch (choice){
            case 1:
                toUpperCase(fileChosen);
                break;
            case 2:
                toLowerCase(fileChosen);
                break;
            case 3:
                break;
            default:
                System.out.println("Please enter a valid operation.");
        }
    }
}
