package main.archivos;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Created by Florencia on 6/12/17.
 */
public class Ejercicio4 {

    public static void create(){
        try{
            FileWriter paises = new FileWriter("Paises");
            paises.write("el salvador;4500000;25.85 Billion;\n");
            paises.write("francia;66500000;2.422 Trillion;\n");
            paises.write("alemania;81410000;3.622 Trillion;\n");
            paises.write("reino unido;65400000;2.849 Trillion;\n");
            paises.write("guatemala;16340000;63.41 Billion;\n");
            paises.close();
        }
        catch(IOException e){
            System.out.println(e.getMessage());
        }
    }

    private static void getReport(FileReader fileReader) throws IOException {
        try{
            FileWriter above30 = new FileWriter("Countries above 30 mil");
            FileWriter below30 = new FileWriter("Countries below 30 mil");
            BufferedReader bf = new BufferedReader(fileReader);
            String currentLine = bf.readLine();
            while (currentLine != null){
                String[] line = currentLine.split(";");
                int poblation = Integer.parseInt(line[1]);
                if (poblation <= 30000000){
                    below30.write(currentLine + "\n");
                } else {
                    above30.write(currentLine + "\n");
                }
                currentLine = bf.readLine();
            }
            below30.close();
            above30.close();
        }
        catch(IOException e){
            System.out.println(e.getMessage());
        }
    }

    public static void main(String[] args) throws IOException {
        create();
        FileReader fileChosen = null;
        try {
            fileChosen = new FileReader(Scanner.getString("File name: "));
        } catch (IOException e) {
            System.out.println("ERROR!");
        }
        getReport(fileChosen);
    }
}
