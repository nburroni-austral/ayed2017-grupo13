package main.archivos;

import java.io.*;

/**
 * Created by Florencia on 6/14/17.
 */
public class PracticaParcial {
    public static void ejercicio2(File file){
        try {
            FileReader fileReader = new FileReader(file);
            BufferedReader bf = new BufferedReader(fileReader);
            FileWriter fileWriter = new FileWriter("Result");
            String str = bf.readLine();
            while(str != null){
                fileWriter.write(str.charAt(0) + "\n");

                str = bf.readLine();
            }
            fileWriter.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void create(){
        try{
            FileWriter flor = new FileWriter("flor");
            flor.write("hola.\n");
            flor.write("Flor \n");
            flor.write("apu");
            flor.close();
        }
        catch(IOException e){
            System.out.println(e.getMessage());
        }
    }

    public static void main(String[] args) throws IOException {
        create();
        File fileChosen = null;
        fileChosen = new File(Scanner.getString("File name: "));
        ejercicio2(fileChosen);
    }
}
