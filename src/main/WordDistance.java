package main;

/**
 * Created by Florencia on 3/17/17.
 */
public class WordDistance {
    private String word1;
    private String word2;

    public WordDistance(String word1, String word2) {
        this.word1 = word1;
        this.word2 = word2;
    }

    /*@
     * @param word1.length == word2.length;
     * @*/

    public int hamming(String word1, String word2){
        if(word1.length() != word2.length()){
            return -1;
        }
        int count = 0;
        for (int i = 0; i < word1.length(); i++) {
            if(word1.length() == i || word2.length() == i){
                return count;
            }
            if(word1.charAt(i) != word2.charAt(i)){
                count++;
            }
        }
        return count;
    }

    public int levenshtein(String word1, String word2) {
        word1 = word1.toLowerCase();
        word2 = word2.toLowerCase();
        int[] costs = new int[word2.length() + 1];
        for (int j = 0; j < costs.length; j++)
            costs[j] = j;
        for (int i = 1; i <= word1.length(); i++) {
            costs[0] = i;
            int aux = i - 1;
            for (int j = 1; j <= word2.length(); j++) {
                if (word1.charAt(i - 1) != word2.charAt(j - 1)){
                    aux += 1;
                }
                int value = Math.min(1 + Math.min(costs[j], costs[j - 1]), aux);
                aux = costs[j];
                costs[j] = value;
            }
        }
        return costs[word2.length()];
    }
}
