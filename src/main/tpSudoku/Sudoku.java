package main.tpSudoku;

import main.stacks.DynamicStack;

/**
 * Created by Florencia on 3/31/17.
 */
public class Sudoku {
    private IntSpot[][] table;
    private int[] posibleNumbersArray = {1,2,3,4,5,6,7,8,9};


    public Sudoku(IntSpot[][] initialBoard) {
        table = initialBoard;
    }

    private void solveRow(IntSpot spot, int row){
        for(int i = 0; i < 9; i++){
            for (int j = 0; j < 9; j++) {
                if(posibleNumbersArray[j] == table[row][i].getValue()){
                    posibleNumbersArray[j] = -1;
                }
            }

        }
    }

    private void solveColumn(IntSpot spot,int column){
        for(int i = 0; i < 9; i++){
            for (int j = 0; j < 9; j++) {
                if(posibleNumbersArray[j] == table[i][column].getValue()){
                    posibleNumbersArray[j] = -1;
                }
            }

        }
    }

    private void solveSquare(int row, int column){
        int x1 = 3 * (row/3);
        int y1 = 3 * (column/3);
        int x2 = x1 + 2;
        int y2 = y1 + 2;
        for(int i = x1; i <= x2; i++){
            for (int j = y1; j <= y2; j++) {
                if(table[i][j] != null && table[i][j].getValue()!= -1){
                    int numero = table[i][j].getValue();
                    for (int k = 0; k < posibleNumbersArray.length; k++){
                        if (posibleNumbersArray[k]==numero){
                            posibleNumbersArray[k] = -1;
                        }
                    }
                }
            }

        }
    }

    private DynamicStack<Integer> createStack(){
        DynamicStack<Integer> stack = new DynamicStack<>();
        for (int i=0; i<posibleNumbersArray.length; i++){
            if (posibleNumbersArray[i] != -1){
                stack.push(posibleNumbersArray[i]);
            }
        }
        return stack;
    }

    public int[][] solve(int[][] board){
        table = changeToSpot(board);
        int i = 0;
        int j = 0;
        while (i <= 8){
            while (j <= 8){
                if (table[i][j].getValue() <= 0){
                    solveColumn(table[i][j],j);
                    solveRow(table[i][j],i);
                    solveSquare(i,j);
                    if (allFull(posibleNumbersArray)){
                        int[] newPosition = getPreviousStack(i,j);
                        i = newPosition[0];
                        j = newPosition[1];
                        table[i][j].posibleNumbers.pop();
                        table[i][j].value = table[i][j].posibleNumbers.peek();
                    }else{
                        table[i][j].setPosibleNumbers(createStack());
                        table[i][j].value = table[i][j].posibleNumbers.peek();
                    }
                }
                posibleNumbersArray = new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9};
                j++;
            }
            j = 0;
            i++;
        }
        return changeFromSpot(table);
    }

    public int[] goBack(int i, int j){
        if (j-1<0){
            i--;
            j=8;
            return new int[] {i,j};
        }else{
            j--;
            return new int[] {i,j};
        }
    }

    public int[] getPreviousStack(int i, int j){
        int[] newPosition = goBack(i,j);
        int k = newPosition[0];
        int h = newPosition[1];
        while (table[k][h].posibleNumbers == null || table[k][h].posibleNumbers.size() <= 1  ) {
            if (!table[k][h].isInitialized()) {
                table[k][h].value = -1;
                int[] previousPosition = goBack(k, h);
                k = previousPosition[0];
                h = previousPosition[1];
            }else {
                int[] previousPosition = goBack(k, h);
                k = previousPosition[0];
                h = previousPosition[1];
            }
        }
        return new int[] {k,h};

    }

    public IntSpot[][] changeToSpot(int[][] board){
        IntSpot[][] newBoard = new IntSpot[9][9];
        int i = 0;
        int j = 0;
        while (i <= 8) {
            while (j <= 8) {
                if (board[i][j] == -1) {
                    newBoard[i][j] = new IntSpot(board[i][j]);
                }else {
                    newBoard[i][j] = new IntSpot(board[i][j]);
                    newBoard[i][j].setInitialized(true);
                }
                j++;
            }
            j = 0;
            i++;
        }
        return newBoard;
    }

    public int[][] changeFromSpot(IntSpot[][] board){
        int[][] newBoard = new int[9][9];
        int i = 0;
        int j = 0;
        while (i <= 8) {
            while (j <= 8) {
                newBoard[i][j] = board[i][j].value;
                j++;
            }
            j = 0;
            i++;
        }
        return newBoard;
    }

    public boolean allFull(int[] numbers){
        boolean full = true;
        for (int i=0; i<numbers.length; i++){
            if (numbers[i] != -1){
                full = false;
            }
        }
        return full;
    }
}
