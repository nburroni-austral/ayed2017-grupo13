package main.tpSudoku;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Florencia on 4/4/17.
 */
public class SudokuController {
    private Sudoku sudoku;
    private SudokuView sudokuView;

    public SudokuController(){
        sudoku = new Sudoku(new IntSpot[9][9]);
        sudokuView = new SudokuView(new SolveSudoku());
    }

    public class SolveSudoku implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            int[][] board = sudokuView.getValues();
            int[][] solvedBoard = sudoku.solve(board);
            sudokuView.setValues(solvedBoard);
        }
    }
}
