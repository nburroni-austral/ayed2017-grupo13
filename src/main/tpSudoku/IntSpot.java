package main.tpSudoku;

import main.stacks.DynamicStack;

/**
 * Created by Florencia on 3/31/17.
 */
public class IntSpot {
    private int row;
    private int column;
    public int value;
    public DynamicStack<Integer> posibleNumbers;
    boolean initialized = false;

    public IntSpot(int value) {

        this.value = value;
    }

    public void setPosibleNumbers(DynamicStack<Integer> posibleNumbers) {
        this.posibleNumbers = posibleNumbers;
    }

    public int getRow() {
        return row;
    }

    public int getColumn() {
        return column;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public void setInitialized(boolean initialized) {
        this.initialized = initialized;
    }

    public boolean isInitialized() {
        return initialized;
    }
}
