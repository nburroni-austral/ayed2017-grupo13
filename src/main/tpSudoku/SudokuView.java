package main.tpSudoku;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

/**
 * Created by Tincho on 04-Apr-17.
 */
public class SudokuView extends JFrame {
    private JPanel panel = new JPanel();
    JTextField[][] square = new JTextField[9][9];
    JButton solve = new JButton("Solve");
    int [][] numbersArray = new int[9][9];

    public SudokuView(ActionListener solveButton) {
        super("Sudoku");
        setSize(700,700);
        setLocationRelativeTo(null);
        setResizable(false);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        panel.setLayout(new GridLayout(9,9));
        int i = 0;
        int j = 0;
        while (i <= 8) {
            while (j <= 8) {
                square[i][j] = new JTextField();
                square[i][j].setHorizontalAlignment(JTextField.CENTER);
                square[i][j].setFont(new Font("arial",0,16));
                panel.add(square[i][j]);
                j++;
            }
            j = 0;
            i++;
        }
        panel.setSize(500,500);
        add(panel);
        //add(BorderLayout.NORTH,panel);
        solve.setSize(50,50);
        solve.addActionListener(solveButton);
        add(BorderLayout.SOUTH,solve);

        setVisible(true);
    }

    public int[][] getValues(){
        int i = 0;
        int j = 0;
        while (i <= 8) {
            while (j <= 8) {
                if (!square[i][j].getText().equals("")){
                    numbersArray[i][j] = Integer.parseInt(square[i][j].getText());
                }else {
                    numbersArray[i][j] = -1;
                }
                j++;
            }
            j = 0;
            i++;
        }
        return numbersArray;
    }

    public void setValues(int[][] table){
        int i = 0;
        int j = 0;
        while (i <= 8) {
            while (j <= 8) {
                square[i][j].setText("" + table[i][j]);
                j++;
            }
            j = 0;
            i++;
        }
    }
}
