package main.trees;

import main.trees.Vimberg.DoubleNode;
import main.trees.Vimberg.DynamicBinaryTree;
import struct.istruct.BinaryTree;

/**
 * Created by Florencia on 4/10/17.
 */
public class BinaryTreeEj13<T> {
    /*
    Utilizando las primitivas de árbol binario escribir los métodos para calcular:
    a) el peso.
    b) el número de hojas.
    c) el número de veces que aparece un elemento dado.
    d) el número de elementos que tienen el árbol en un nivel dado.
    e) la altura.
    */



    public int weight(BinaryTree<T> tree){
        if(tree.isEmpty()){
            return 0;
        } else {
            return (1+ weight( tree.getLeft())+ weight( tree.getRight())) ;
        }
    }

//    public int nodes(DynamicBinaryTree tree){
//        if(tree.isEmpty()){
//            return 0;
//        }
//        return 0;
//    }

    public int leaves(DynamicBinaryTree<T> tree){ //Por que casteo excesivo??
        if(tree.isEmpty()){
            return 0;
        }
        if(tree.getRight().isEmpty() && tree.getLeft().isEmpty()){
            return 1;
        } else {
            return leaves((DynamicBinaryTree<T>) tree.getLeft())+ leaves((DynamicBinaryTree<T>) tree.getRight());
        }
    }

    public int height(DynamicBinaryTree tree){
        int height = 0;
        height((DoubleNode) tree.getRoot(), 1, 0);
        return height;
    }

    private void height(DoubleNode node, int level, int height) {
        if (node != null) {
            height(node.getLeft(), level + 1, height);
            if (level > height) {
                height = level;
            }
            height(node.getRight(), level + 1, height);
        }
    }

    public int elementsAtLevel(DynamicBinaryTree tree, int level){
        if(level == 0){
            return 0;
        } if(level == 1){
            return 1;
        }
        return elementsAtLevel((DynamicBinaryTree) tree.getLeft(), level-1) +
                elementsAtLevel((DynamicBinaryTree) tree.getRight(), level-1);
    }


    public int amountTimesElement(DynamicBinaryTree tree, int element){
        int count = 0;
        if (tree.getRoot().equals(element)){
            count++;
        } if(tree.isEmpty()){
            return 0;
        } else
            return count + amountTimesElement((DynamicBinaryTree) tree.getLeft(), element) +
                amountTimesElement((DynamicBinaryTree) tree.getRight(),element);
    }

    public void preorder(DynamicBinaryTree tree){
        if(tree.isEmpty()) {
            preorder((DynamicBinaryTree) tree.getLeft());
            preorder((DynamicBinaryTree) tree.getRight());
        }
    }
}
