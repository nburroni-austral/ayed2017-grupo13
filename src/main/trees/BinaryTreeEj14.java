package main.trees;

import main.trees.Vimberg.DynamicBinaryTree;
import struct.istruct.BinaryTree;

import java.util.ArrayList;

/**
 * Created by Florencia on 4/10/17.
 */
public class BinaryTreeEj14 {
    BinaryTreeEj13 ej13 = new BinaryTreeEj13();

    public int sumElements(DynamicBinaryTree<Integer> tree){
        return sumElements(tree,0);
    }

    private int sumElements(DynamicBinaryTree<Integer> tree, int count){
        count += tree.getRoot();
        return sumElements((DynamicBinaryTree<Integer>) tree.getLeft(),count) +
                sumElements((DynamicBinaryTree<Integer>) tree.getRight(),count);
    }

    public int sumMultiplos3(DynamicBinaryTree<Integer> tree){
        return sumMultiplos3(tree,0);
    }

    private int sumMultiplos3(DynamicBinaryTree<Integer> tree, int count){
        if(tree.getRoot() % 3 == 0){
            count += tree.getRoot();
            return sumElements((DynamicBinaryTree<Integer>) tree.getLeft(),count) +
                    sumElements((DynamicBinaryTree<Integer>) tree.getRight(),count);
        }
        return count;
    }

    public boolean equals(BinaryTree<Integer> tree1, BinaryTree<Integer> tree2){
        boolean result = true;
        if(tree1.getRoot() == tree2.getRoot()){
            result = equals(tree1.getLeft(), tree2.getLeft()) || equals(tree1.getRight(), tree2.getRight());
        }
        return result;
    }

    //Dos árboles binarios son isomorfos si tienen la misma
    //estructura aunque el contenido de cada uno de sus nodos sea diferente.


    public boolean isomorfos( DynamicBinaryTree tree1, DynamicBinaryTree tree2 ){
        return true;
    }

    public boolean semejantes(BinaryTree<Integer> tree1, BinaryTree<Integer> tree2){
        return true;
    }

    public boolean completo(BinaryTree<Integer> tree){
        if(tree.isEmpty()){
            return false;
        }
        if (tree.getLeft().isEmpty()){
            return completo(tree.getRight());
        }
        if (tree.getRight().isEmpty()){
            return completo(tree.getLeft());
        }
        return true;
    }

//    public boolean lleno(BinaryTree<Integer> tree){
//
//    }

    /* Un árbol de valores enteros es estable si es vacío, consta de un único elemento
    o para todo elemento de la estructura su padre es mayor. */
    public boolean estable(BinaryTree<Integer> tree){
        if(tree.isEmpty()){
            return true;
        }
        if( tree.getLeft().isEmpty() && tree.getRight().isEmpty()){
            return true;
        }
        if( tree.getRoot() < tree.getLeft().getRoot() || tree.getRoot() < tree.getRight().getRoot()){
            return false;
        }
        return estable(tree.getLeft()) && estable(tree.getRight());
    }

    public boolean ocurre(BinaryTree tree1, BinaryTree tree2){
            if(tree1.isEmpty()) {
                return false;
            }
            if(tree1.getRoot().equals(tree2.getRoot())) {
                return ocurre(tree1.getLeft(), tree2) || ocurre(tree1.getRight(), tree2);
            }
            return false;
    }

    public void mostrarFrontera(BinaryTree tree){
        if(tree.getLeft().isEmpty() && tree.getRight().isEmpty()){
            System.out.println("Elemento en la frontera" + tree.getRoot());
        }
    }

    private ArrayList frontera(BinaryTree tree, ArrayList frontera){
        if(tree.getLeft().isEmpty() && tree.getRight().isEmpty()){
            frontera.add(tree.getRoot());
        }
        return frontera;
    }

    public ArrayList frontera(BinaryTree tree){
        ArrayList frontera = new ArrayList();
        frontera(tree.getLeft(), frontera);
        frontera(tree.getRight(), frontera);
        return frontera(tree, frontera);

    }

    private boolean mismaEstructura(BinaryTree tree1, BinaryTree tree2){
        if(tree1.isEmpty() && tree2.isEmpty()){
            return true;
        }
        if(tree1.isEmpty() || tree2.isEmpty()){
            return false;
        }
        return mismaEstructura(tree1.getLeft(),tree2.getLeft()) &&
                mismaEstructura(tree1.getRight(), tree2.getRight());
    }

//    private boolean mismosElementos(BinaryTree tree1, BinaryTree tree2){
//
//    }
}
