package main.trees;

import main.trees.Vimberg.DynamicBinaryTree;
import struct.istruct.BinaryTree;

/**
 * Created by Florencia on 4/24/17.
 */
public class Practice<T> {
    private boolean isInLeaves(T element,BinaryTree<T> binaryTree){
        if (binaryTree.isEmpty())return false;
        if (binaryTree.getLeft().isEmpty()&& binaryTree.getRight().isEmpty()){
            if (element.equals(binaryTree.getRoot())){
                return true;
            }
            else return false;
        }
        else {
            return isInLeaves(element,binaryTree.getRight())|| isInLeaves(element,binaryTree.getLeft());
        }

    }


    private BinaryTree<Integer> sumAndIsomorphic(BinaryTree<Integer> binaryTree){

        if (binaryTree.isEmpty()) return new DynamicBinaryTree<>();

        int sum= sumWithItsDecendants(binaryTree);

        DynamicBinaryTree<Integer> leftTree= (DynamicBinaryTree<Integer>) sumAndIsomorphic(binaryTree.getLeft());

        DynamicBinaryTree<Integer> rightTree= (DynamicBinaryTree<Integer>) sumAndIsomorphic(binaryTree.getRight());

        return new DynamicBinaryTree<Integer>(sum,leftTree,rightTree);
    }



    private int sumWithItsDecendants(BinaryTree<Integer> binaryTree){ //check
        int result = 0;
        if (binaryTree.isEmpty())return result;
        if (binaryTree.getLeft().isEmpty()||binaryTree.getRight().isEmpty()){
            result += binaryTree.getRoot() + binaryTree.getRight().getRoot();
        }
        else{
            result+= binaryTree.getRoot();

        }
        return result + sumWithItsDecendants(binaryTree.getLeft())+sumWithItsDecendants(binaryTree.getRight());
    }
}
