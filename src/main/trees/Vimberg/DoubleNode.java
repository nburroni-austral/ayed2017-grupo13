package main.trees.Vimberg;

/**
 * Created by Florencia on 4/10/17.
 */
public class DoubleNode<T> {
    T data;
    DoubleNode <T> right, left;

    public DoubleNode(T o){
        data = o;
    }
    public DoubleNode(T o, DoubleNode<T> left, DoubleNode<T> right){
        data = o;
        this.right = right;
        this.left = left;
    }

    public DoubleNode<T> getRight() {
        return right;
    }

    public DoubleNode<T> getLeft() {
        return left;
    }

    public T getData() {
        return data;
    }
//
//    @Override
//    public String toString() {
//        return "DoubleNode{" +
//                "data=" + data +
//                ", right=" + right +
//                ", left=" + left +
//                '}';
//    }
}
