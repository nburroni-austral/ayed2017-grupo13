package main.trees.Vimberg;

/**
 * Created by Florencia on 4/25/17.
 */
public class Account implements Comparable<Account> {
    private int accountNumber;
    private String sucursal; //A or B

    public Account(int accountNumber, String sucursal) {
        this.accountNumber = accountNumber;
        this.sucursal = sucursal;
    }

    public int getAccountNumber() {
        return accountNumber;
    }

    public String getSucursal() {
        return sucursal;
    }

    @Override
    public int compareTo(Account o) {
        return o.getSucursal().compareTo(this.getSucursal());
    }

    @Override
    public String toString() {
        return "Account{" +
                "accountNumber=" + accountNumber +
                ", sucursal='" + sucursal + '\'' +
                '}';
    }
}
