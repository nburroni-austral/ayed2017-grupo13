package main.trees.Vimberg;

import struct.istruct.BinaryTree;

/**
 * Created by Florencia on 4/25/17.
 */
public class Bank {
    private BinaryTree<Account> accounts;

    public Bank(BinaryTree<Account> accounts) {
        this.accounts = accounts;
    }

    public void createNewBinaryTrees(){
        fillTrees(new ArBinBus<>(), new ArBinBus<>(), accounts);
    }

    private void fillTrees(ArBinBus<Account> sucursalA, ArBinBus<Account> sucursalB, BinaryTree<Account> accounts){
        if(accounts.getRoot().getSucursal().equals("A")){
            sucursalA.insertar(accounts.getRoot());
            fillTrees(sucursalA, sucursalB, accounts.getLeft());
            fillTrees(sucursalA, sucursalB, accounts.getRight());
        } else if(accounts.getRoot().getSucursal().equals("B")){
            sucursalB.insertar(accounts.getRoot());
            fillTrees(sucursalA, sucursalB, accounts.getLeft());
            fillTrees(sucursalA, sucursalB, accounts.getRight());
        }
    }
}
