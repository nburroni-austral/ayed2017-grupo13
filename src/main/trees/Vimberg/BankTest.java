package main.trees.Vimberg;

/**
 * Created by Florencia on 4/25/17.
 */
public class BankTest {
    public static void main(String[] args) {
        Account account1 = new Account(121212, "A");
        Account account2 = new Account(222222, "A");
        Account account3 = new Account(232323, "B");
        Account account4 = new Account(444212, "B");
        Account account5 = new Account(623421, "A");

        DynamicBinaryTree<Account> accounts = new DynamicBinaryTree<>();

        Bank bank = new Bank(accounts);

        System.out.println(accounts.getRoot());

        //bank.createNewBinaryTrees();
    }
}
