package main.trees.Vimberg;

import struct.istruct.BinaryTree;

/**
 * Created by Florencia on 4/10/17.
 */
public class DynamicBinaryTree<T> implements BinaryTree<T>{
    private DoubleNode<T> root;

    public DynamicBinaryTree(){
        root = null;
    }

    public DynamicBinaryTree(T o){
        root = new DoubleNode(o);
    }

    public DynamicBinaryTree(T o, DynamicBinaryTree<T> left, DynamicBinaryTree<T> right){
        root = new DoubleNode(o, left.root, right.root);
    }

    @Override
    public boolean isEmpty() {
        if(root == null){
            return true;
        }
        return false;
    }

    @Override
    public T getRoot() {
        if(isEmpty()){
            return null;
        }
        return root.data;
    }

    public BinaryTree<T> getLeft(){
        DynamicBinaryTree<T> t = new DynamicBinaryTree<T>();
        t.root = root.left;
        return t;
    }

    public BinaryTree<T> getRight(){
        DynamicBinaryTree t = new DynamicBinaryTree();
        t.root = root.right;
        return t;

    }
}
