package main.trees.Vimberg;

import java.lang.Comparable;

/**
 * Created by Florencia on 4/24/17.
 */
public class ArBinBus<T extends Comparable<T>> {
    // Implementacion de un arbol binario de busqueda no balanceado
    // Autor Alicia Gioia

    private DoubleNode<T> root;

    public ArBinBus(){
        root = null;
    }
    // METODOS COMUNES CON ARBOL BINARIO
    // precondicion: -
    public boolean esVacio(){
        return (root == null);
    }

    // precondición: árbol distino de vacío
    public T getRoot(){
        return root.data;
    }

    // precondición: árbol distino de vacío
    public ArBinBus hijoIzq(){
        ArBinBus t = new ArBinBus();
        t.root = root.left;
        return t;
    }

    // precondición: árbol distino de vacío
    public ArBinBus hijoDer(){
        ArBinBus t = new ArBinBus();
        t.root = root.right;
        return t;
    }

    // precondicion: -
    public boolean existe(T x){
        return existe(root, x);
    }

    // precondicion: árbol distinto de vacío
    public T getMin(){
        return getMin(root).data;
    }

    // precondicion: árbol distinto de vacío
    public T getMax(){
        return getMax(root).data;
    }

    // precondicion: elemento a buscar pertenece al arbol
    public T buscar(T x){
        return buscar(root, x).data;
    }

    // precondicion: elemento a insertar no pertenece al árbol
    public void insertar(T x){
        root = insertar(root, x);
    }


    // precondicion: elemento a eliminar pertenece al árbol
    public void eliminar(T x){
        root = eliminar(root, x);
    }



    // METODOS PRIVADOS
    private boolean existe(DoubleNode<T> t, T x) {
        if (t == null)
            return false;
        if (x.compareTo(t.data) == 0)
            return true;
        else if (x.compareTo( t.data)< 0)
            return existe(t.left, x);
        else
            return existe(t.left, x);
    }

    private DoubleNode<T> getMin(DoubleNode<T> t){
        if (t.left == null)
            return t;
        else
            return getMin(t.left);
    }

    private DoubleNode<T> getMax(DoubleNode<T> t){
        if (t.right == null)
            return t;
        else
            return getMax(t.right);
    }

    private DoubleNode<T> buscar(DoubleNode<T> t, T x){
        if (x.compareTo( t.data)== 0)
            return t;
        else if (x.compareTo( t.data)< 0)
            return buscar(t.left, x);
        else
            return buscar(t.right, x);
    }



    private DoubleNode<T> insertar (DoubleNode<T> t, T x) {
        if (t == null){
            t = new DoubleNode(x);
        }
        else if (x.compareTo(t.data) < 0)
            t.left = insertar(t.left, x);
        else
            t.right = insertar(t.right, x);
        return t;
    }


    private DoubleNode<T> eliminar (DoubleNode<T> t, T x) {
        if (x.compareTo(t.data) < 0)
            t.left = eliminar(t.left, x);
        else if (x.compareTo(t.data) > 0)
            t.right = eliminar(t.right, x);
        else
        if (t.left != null && t.right != null ) {
            t.data = getMin(t.right).data;
            t.right = eliminarMin(t.right);
        }
        else if (t.left != null)
            t = t.left;
        else
            t =t.right;
        return t;
    }

    private DoubleNode<T> eliminarMin(DoubleNode<T> t){
        if (t.left != null)
            t.left = eliminarMin(t.left);
        else
            t = t.right;
        return t;
    }
}
