package main.trees;

import main.trees.Vimberg.ArBinBus;

/**
 * Created by Florencia on 4/24/17.
 */
public class ElectricCompany {
    private ArBinBus<Bulb> bulbs;

    private ElectricCompany(){
        bulbs = new ArBinBus<>();
    }

    public void alta(Bulb bulb){
        bulbs.insertar(bulb);
    }

    public void alta(Bulb bulb, int amount){
        for (int i = 0; i < amount; i++) {
            bulbs.insertar(bulb); //?
        }
    }
}
