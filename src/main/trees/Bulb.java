package main.trees;


/**
 * Created by Florencia on 4/24/17.
 */
public class Bulb implements Comparable<Bulb> {
    private String bulbCode;
    private int watts;
    private String bulbType;
    private int quantity;

    public Bulb(String bulbCode, int watts, String bulbType, int quantity) {
        if (bulbCode.length() < 5 && bulbType.length() < 10){
            this.bulbCode = bulbCode;
            this.watts = watts;
            this.bulbType = bulbType;
            this.quantity = quantity;
        }else {
            System.out.println("Lampara " + bulbCode + " invalida");
        }

    }

    public Bulb(String bulbCode) {
        this.bulbCode = bulbCode;
    }

    public String getBulbCode() {
        return bulbCode;
    }

     @Override
    public int compareTo(Bulb x) {
         Bulb bulb = (Bulb) x;
         return bulb.getBulbCode().compareTo(this.bulbCode);
    }
}
