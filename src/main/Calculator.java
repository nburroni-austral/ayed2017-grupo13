package main;

import main.stacks.DynamicStack;

/**
 * Created by Florencia on 3/27/17.
 */
public class Calculator {
    private DynamicStack<Character> stack = new DynamicStack<>();

    public double calculate(String toCalculate){
        char[] calculate = toCalculate.toCharArray();
        double result = 0;
        for(int i = calculate.length-1 ; i >= 0; i--){
            stack.push(calculate[i]);
        }
        while(!stack.isEmpty()) {
            while (!stack.isEmpty() && (!isSum( stack.peek()) || !isSubtraction( stack.peek()))) {
                double partialResult = 0;
                int intNumber = 0;
                if (!isDivision( stack.peek()) && !isMultiplication( stack.peek())) {
                    String number = "";
                    number += stack.peek();
                    stack.pop();
                    while (!stack.isEmpty() && Character.isDigit(stack.peek())) {
                        number = number + stack.peek();
                        stack.pop();
                    }
                    intNumber = Integer.parseInt(number);
                    partialResult += intNumber;
                }
                while (!stack.isEmpty() && (isDivision(stack.peek()) || isMultiplication(stack.peek()))) {
                    if (isMultiplication(stack.peek())) {
                        stack.pop();
                        String number2 = "";
                        int intNumber2;
                        number2 += stack.peek();
                        stack.pop();
                        while (!stack.isEmpty() && Character.isDigit(stack.peek())) {
                            number2 = number2 + stack.peek();
                            stack.pop();
                        }
                        intNumber2 = Integer.parseInt(number2);
                        partialResult = intNumber * intNumber2;
                    } else if (isDivision(stack.peek())) {
                        stack.pop();
                        String number2 = "";
                        int intNumber2;
                        number2 += stack.peek();
                        stack.pop();
                        while (!stack.isEmpty() && Character.isDigit(stack.peek())) {
                            number2 = number2 + stack.peek();
                            stack.pop();
                        }
                        intNumber2 = Integer.parseInt(number2);
                        partialResult = intNumber / intNumber2;
                    }
                }
                result += partialResult;
            }
        }
        return result;
    }

    public boolean isDivision(char a){
        if(a == '/'){
            return true;
        }
        return false;
    }

    public boolean isMultiplication(char a){
        if(a == '*'){
            return true;
        }
        return false;
    }

    public boolean isSum(char a){
        if(a == '+'){
            return true;
        }
        return false;
    }

    public boolean isSubtraction(char a){
        if(a == '-'){
            return true;
        }
        return false;
    }
}
